var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

//const doc = require('dynamodb-doc');
//const dynamoRequest = new doc.DynamoDB();
//const dynamoRequest = new AWS.DynamoDB();
async function getAllTags() {


    var user_id = "3";
    var sharing_gems_params = {
        TableName: "sharing_requests",
        IndexName: "user_id-index",
        KeyConditionExpression: "user_id = :user_id",
        //  ExpressionAttributeNames: { "#user_id": "user_id" },
        ExpressionAttributeValues: { ":user_id": user_id }
    };

    //      var params = {
    //   ExpressionAttributeValues: {
    //     ':s': 2,
    //     ':e': 9,
    //     ':topic': 'PHRASE'
    //   },
    //  KeyConditionExpression: 'Season = :s and Episode > :e',
    //  FilterExpression: 'contains (Subtitle, :topic)',
    //  TableName: 'EPISODES_TABLE'
    // };
    try {
        const data = await docClient.query(sharing_gems_params).promise();
        return { statusCode: 200, body: JSON.stringify({ sharing_gems_params, data }) };
    }
    catch (error) {
        return {
            statusCode: 400,
            error: `Could not post: ${error.stack}`
        };
    }

    // await docClient.query(sharing_gems_params, function(err, data) {
    //   if (err) {
    //     console.log("Error", err);
    //     return "test";
    //   } else {
    //     console.log("Success", data.Items);
    //     return "test";
    //   }
    // });

}

exports.getUsersNotifications = async(user_email) => {
    // TODO implement
    return await getAllTags();

};


module.exports = function() {
    //  this.sum = function(a, b) { return a + b };

this.getUserEmailFromId = async(user_id) => {

        var sharing_gems_params = {
            TableName: "users",
            IndexName: "user_id-index",
            KeyConditionExpression: "user_id = :user_id",
            ExpressionAttributeValues: { ":user_id": user_id }
        };

        try {
            const data = await docClient.query(sharing_gems_params).promise();
            if (data.Items && data.Items.length == 1) {
                var userInfo = data.Items[0];
                var userId = userInfo.email_id;
                return { error: false, body: userId };
            }
            else if (data.Items && data.Items.length > 1) {
                return { error: false, reason: "Too many items found." };
            }
            else {
                return { error: true, reason: "No items found." };
            }
        }
        catch (error) {
            console.log(`Could not post: ${error.stack}`);
            return {
                error: true,
                reason: "DB error"
                //  error: `Could not post: ${error.stack}`
            };
        }

        // return await getAllTags();

    };


    this.getListOfRequestsForUserEmail = async(user_email) => {

        var user_id_from_email_result = await getUserIdFromEmail(user_email);
        if (user_id_from_email_result.error == true) {
            return user_id_from_email_result;
        }
        else {
            var user_id_from_email = user_id_from_email_result.test;
            var user_id_from_email_result = await getListOfRequestsForUserId(user_id_from_email);
        }

    };

    this.getListOfRequestsForUserId = async(user_id) => {

        var sharing_gems_params = {
            TableName: "sharing_requests",
            IndexName: "user_id-index",
            KeyConditionExpression: "user_id = :user_id",
            ExpressionAttributeValues: { ":user_id": user_id, "pending": true },
            FilterExpression: 'contains (pending, :pending)', //'{"#v": "Views"}
            ExpressionAttributeNames: { "#user_id": "user_id" },


        };

        try {
            const data = await docClient.query(sharing_gems_params).promise();
            if (data.Items && data.Items.count > 0) {
                return { error: false, body: data.Items };
            }
            else {
                return { error: true, reason: "No items found." };
            }
        }
        catch (error) {
            return {
                error: true,
                reason: "DB error"
                //  error: `Could not post: ${error.stack}`
            };
        }

        // return await getAllTags();

    };
}
