var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

require('./mega_logger.js')();
/*global  MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/

/*global DB_GEMS*/
module.exports = function() {

    this.GetGem = async(gem_id) => {
        return await getGem(gem_id);
    };

    this.WriteGem = async(real_gem_id, user_id) => {
        return await writeGem(real_gem_id, user_id);
    };

    this.GetGemOwnerID = async(gem_id, gem_data) => {
        return await getGemOwnerID(gem_id, gem_data);
    };
    this.AmITheGemOwner = async(my_user_id, gem_id, gem_data) => {
        return await amITheGemOwner(my_user_id, gem_id, gem_data);
    }

    this.GetRealGemID = async(gem_id_from_app) => {
        return await getRealGemID(gem_id_from_app);
    };

}

async function amITheGemOwner(my_user_id, gem_id, gem_data) {

    var owner_id = await getGemOwnerID(gem_id, gem_data);

    if (owner_id === my_user_id) {
        MegaLog("Gem is ours");
        return true;
    }
    else {
        MegaLog("Gem is NOT ours");
        return false;
    }
}

async function getGemOwnerID(gem_id, gem_data) {

    if (gem_data === undefined) {
        MegaLog("getting gem_item because it was undefined");
        gem_data = await getGem(gem_id);
        if (NoErrors() != true) {
            MegaLog("found some errors");
            return;
        }
    }

    var gem_owner_id = gem_data["active_user_id"];
    if (gem_owner_id === undefined) {
        //we should probably log the error where field active_user_id is not present but that is nearly impossible
        // POSSIBLE HAHA! When gem is completely new (but its still very unlikely)
        MegaLog("no active user id!!!!!!!");
        return 0;
    }
    else {
        MegaLog("gem_owner:" + gem_owner_id);
        return gem_owner_id;
    }

}

async function getGem(gem_id) {
    MegaLog("Starting dynamo request for gems table. gem_id:"+gem_id);
    var params = {
        TableName: "gems",
        Key: { "gem_id": gem_id }
    };

    try {
        const data = await docClient.get(params).promise();
        if (data.Item) {
            // MegaLog(params,data.Item);
            return data.Item;
        }
        else {
            MegaLogError("data.Item is not present.No gem is present , this usually should not happen since gemWriter already created the gem for us"); //ERROR_GEM_NOT_FOUND
            return;
        }
    }
    catch (error) {
        MegaLogError(error, "DB error");
        return;
    }

}

async function writeGem(real_gem_id, user_id) {

    var params = {
        Key: {
            "gem_id": real_gem_id
        },
        TableName: DB_GEMS,
        UpdateExpression: 'set active_user_id=:active_user_id',
        ExpressionAttributeValues: {
            ':active_user_id': user_id
        },
        ReturnValues: 'ALL_NEW' //UPDATED_NEW
    };

    MegaLog("params",['Params for writing record :', params]);
    try {
        const data = await docClient.update(params).promise();
        MegaLog("UpdateGem succeeded:", JSON.stringify(data, null, 2));
        //WriteRecord();
        //SUCCESS_OPERATION_SUCCESSFULL
        return;
    }
    catch (error) {
        MegaLogError("ERROR_DB_PROBLEM", error, "Unable to update item");
        return;
    }

}


async function getRealGemID(gem_id_from_app) {

    if (gem_id_from_app.startsWith("OLD:")) { // gem is from the previous anroid version.
        var msgID = gem_id_from_app.replace('OLD:', '');

        var params = {
            TableName: "gems_connection",
            Key: { "message_id": msgID }
        };

        try {
            const data = await docClient.get(params).promise();
            if (data.Item && data.Item.gem_id) {
                var gemid = data.Item.gem_id;
                MegaLog('calculated real id:', gemid, ' from message:', gem_id_from_app);
                return gemid;
            }
            else {
                MegaLogError("data.Item is not present");
                return;
            }
        }
        catch (error) {
            MegaLogError(error, "DB error");
            return;
        }

    }
    else { // gem is from gem writer
        return gem_id_from_app;
    }

}
