var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

require('./mega_logger.js')();
/*global  MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/

module.exports = function() {
    this.RenameMyGem = async(my_user_id, my_gem_id, gem_new_name) => {
        return await renameMyGem(my_user_id, my_gem_id, gem_new_name);
    };
};

async function renameMyGem(my_user_id, my_gem_id, gem_new_name) {

    var params = {
        Key: {
            "gem_id": my_gem_id,
            "user_id": my_user_id
        },
        TableName: "private_gems",
        UpdateExpression: 'set gem_name=:gem_name',
        ExpressionAttributeValues: {
            ':gem_name': gem_new_name,
        },
        ReturnValues: 'ALL_NEW' //UPDATED_NEW
    };
    
    try {
        const data = await docClient.update(params).promise();
        //MegaLog({data:data});
        return;
    }
    catch (error) {
        MegaLogError({error:error,params:params,comments:"DB error"});
        return;
    }
    //service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err);
    //getMyGemList();


}
