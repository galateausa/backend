var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });


/*global DESCRIPTIONS*/
require('./dynamo.js')();


require('./pusher.js')();

require('./sharing.js')();
/*global GetListOfActiveSharingRequests*/
/*global GetListOfActiveSharingRequestsForOwner*/
/*global GetUserIDForActionOnGem*/
/*global GetListOfSharingRequests*/
/*global IsSharingActiveForMeAndThisGem*/
/*global GetSharingRequestByID*/
/*global RemoveSharingRequest*/
/*global SharingRoleForGem*/
/*global StopSharingGem*/

require('./private_gems.js')();
/*global GetPrivateGem*/
/*global WriteItemsToPrivateGem*/
/*global DeletePrivateGem*/

require('./mega_logger.js')();
/*global  InitMegaLog*/
/*global  RunMegaLog*/
/*global MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/
/*global GetTotalExcecutionTime*/
/*global GetFinalErrorType*/
/*global GetSystemUserName*/
/*global GenerateDefaultRecordInfo*/

/*global  MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/
/*global GetAppShortInfo*/
/*global GetAppName*/
/*global DB_GEMS*/
/*global ERROR_GEM_OWNER_PROBLEM*/
/*global GetSystemUserName*/
/*global GetNewTagName*/
/*global GenerateDefaultRecordInfo*/

require('./record_operations.js')();
/*global MoveMyRecord*/
/*global  RenameMyRecord*/
/*global DeleteMyRecord*/
/*global WriteMyRecord*/
/*global AddRecordToPrivateGem*/
/*global MakeMyRecordPublic*/

/*global ERROR_GEM_IS_LOCKED*/

/*global DB_PRIAVATE_GEMS*/


module.exports = function() {
    //1
    this.GetGem = async(gem_id) => {
        return await getGem(gem_id);
    };

    //2
    this.ChangeOwnerForGem = async({ new_owner, gem_id } = {}) => {
        return await changeOwnerForGem({ new_owner, gem_id });
    };

    //3
    this.GetGemOwnerID = async(gem_id, gem_data) => {
        return await getGemOwnerID(gem_id, gem_data);
    };

    //4
    this.AmITheGemOwner = async(my_user_id, gem_id, gem_data) => {
        return await amITheGemOwner(my_user_id, gem_id, gem_data);
    };

    //5
    this.GetRealGemID = async(gem_id_from_app) => {
        return await getRealGemID(gem_id_from_app);
    };

    //6
    this.GetGemsForUser = async({ user_id } = {}) => {
        return await getGemsForUser({ user_id });
    };

    //7
    this.GetNewTag = async(tag_serial_number) => {
        return await getNewTag(tag_serial_number);
    };

    //8
    this.ActivateNewTag = async({ tag_serial_number, tag_id, user_id, tag_type } = {}) => {
        return await activateNewTag({ tag_serial_number, tag_id, user_id, tag_type });
    };

    //9
    this.LockUnlockGem = async({ previous_owner, gem_id } = {}) => {
        return await lockUnlockGem({ previous_owner, gem_id });
    };

    //10
    this.RenameMyGem = async(my_user_id, my_gem_id, gem_new_name) => {
        return await renameMyGem(my_user_id, my_gem_id, gem_new_name);
    };

    //11
    this.DeleteGem = async({ my_user_id, gem_id } = {}) => {
        return await deleteGem({ my_user_id, gem_id });
    };

    //12
    this.CopyMyGem = async(my_user_id, fisrt_gem_id, second_gem_id) => {
        return await copyMyGem(my_user_id, fisrt_gem_id, second_gem_id);
    };
};

//1
async function getGem(gem_id) {
    var params = {
        TableName: "gems",
        Key: { "gem_id": gem_id }
    };

    try {
        const data = await docClient.get(params).promise();
        if (data.Item) {
            MegaLog({ channel: "big data", data: data.Item, comments: "Got gem data" });
            return data.Item;
        }
        else {
            MegaLogError({ comments: "data.Item is not present.No gem is present , this usually should not happen since gemWriter already created the gem for us" }); //ERROR_GEM_NOT_FOUND
            return;
        }
    }
    catch (error) {
        MegaLogError({ error: error, comments: "DB error" });
        return;
    }

}

//2
async function changeOwnerForGem({ new_owner, gem_id } = {}) {

    var params = {
        Key: {
            "gem_id": gem_id
        },
        TableName: DB_GEMS,
        UpdateExpression: 'set active_user_id=:active_user_id',
        ExpressionAttributeValues: {
            ':active_user_id': new_owner
        },
        ReturnValues: 'NONE' //UPDATED_NEW
    };

    MegaLog({ data: 'Changing onership for gem:' + gem_id + ' to :' + new_owner });
    try {
        const data = await docClient.update(params).promise();
        MegaLog({ data: "ChangeOwnerForGem succeeded:" });
        return;
    }
    catch (error) {
        MegaLogError({ type: "ERROR_DB_PROBLEM", error: error, comments: "Unable to update item.ChangeOwnerForGem" });
        return;
    }

}

//3
async function getGemOwnerID(gem_id, gem_data) {

    if (gem_data === undefined) {
        // MegaLog({ data: "getting gem_item because it was undefined" });
        gem_data = await getGem(gem_id);
        if (NoErrors() != true) {
            MegaLog({ data: "found some errors" });
            return;
        }
    }

    var gem_owner_id = gem_data["active_user_id"];
    if (gem_owner_id === undefined) {
        //we should probably log the error where field active_user_id is not present but that is nearly impossible
        // POSSIBLE HAHA! When gem is completely new (but its still very unlikely)
        MegaLog({ data: "no active user id!!!!!!!" });
        return 0;
    }
    else {
        MegaLog({ data: "gem_owner:" + gem_owner_id });
        return gem_owner_id;
    }

}

//4
async function amITheGemOwner(my_user_id, gem_id, gem_data) {

    var owner_id = await getGemOwnerID(gem_id, gem_data);

    if (owner_id === my_user_id) {
        MegaLog({ data: "I AM the owner of gem: " + gem_id });
        return true;
    }
    else {
        MegaLog({ data: "I am NOT  the owner of the gem: " + gem_id });
        return false;
    }
}

//5
async function getRealGemID(gem_id_from_app) {

    if (gem_id_from_app.startsWith("OLD:")) { // gem is from the previous anroid version.
        var msgID = gem_id_from_app.replace('OLD:', '');

        var params = {
            TableName: "gems_connection",
            Key: { "message_id": msgID }
        };

        try {
            const data = await docClient.get(params).promise();
            if (data.Item && data.Item.gem_id) {
                var gemid = data.Item.gem_id;
                MegaLog('calculated real id:', gemid, ' from message:', gem_id_from_app);
                return gemid;
            }
            else {
                MegaLogError({ comments: "data.Item is not present" });
                return;
            }
        }
        catch (error) {
            MegaLogError({ error: error, comments: "DB error" });
            return;
        }

    }
    else { // gem is from gem writer
        return gem_id_from_app;
    }

}

//6
async function getGemsForUser({ user_id } = {}) {
    var params = {
        TableName: DB_GEMS,
        IndexName: "active_user_id-index",
        KeyConditionExpression: "#active_user_id = :active_user_id",
        ExpressionAttributeNames: {
            "#active_user_id": "active_user_id"
        },
        ExpressionAttributeValues: {
            ":active_user_id": user_id
        }
    };

    try {
        const data = await docClient.query(params).promise();
        MegaLog({ channel: "big data", data: data });
        return data.Items;
    }
    catch (error) {
        MegaLogError({ error: error, comments: "DB error" });
        return;
    }

}

//7
async function getNewTag(tag_serial_number) {

    MegaLog({ channel: "tag_serial_number", data: tag_serial_number });
    MegaLog({ data: "tag_serial_number" });
    MegaLog({ data: tag_serial_number });

    var params = {
        TableName: "activated_tags",
        IndexName: "tag_serial_number-index",
        KeyConditionExpression: "#tag_serial_number = :tag_serial_number",
        ExpressionAttributeNames: {
            "#tag_serial_number": "tag_serial_number"
        },
        ExpressionAttributeValues: {
            ":tag_serial_number": tag_serial_number
        }
    };

    var paramsForTagWriterLogs = {
        TableName: "gem_writer_logs",
        IndexName: "original_gem_id-index",
        KeyConditionExpression: "#original_gem_id = :original_gem_id",
        ExpressionAttributeNames: {
            "#original_gem_id": "original_gem_id"
        },
        ExpressionAttributeValues: {
            ":original_gem_id": tag_serial_number
        }
    };




    try {
        const data = await docClient.query(params).promise();
        MegaLog({ channel: "big data", data: data });

        if (data.Items.length == 0) {
            MegaLog({ data: "no tags in activated_tags table" });

            var counter = 0;
            for (; counter < 2; counter++) {

                MegaLog({ data: "start looking to gem_writer_logs table." });
                var serial_serial = tag_serial_number;

                if (counter == 0) {
                    MegaLog({ data: "Start looking to gem_writer_logs table. Normal serial number order" });
                }
                else if (counter == 1) {
                    if (tag_serial_number.length != 16) {
                        MegaLog({ data: "Reverse serial number cannot be produced" });
                        break; //
                    }
                    serial_serial = "";
                    var step;
                    for (step = 16; step >= 0; step -= 2) {
                        var test_element = tag_serial_number.substring(step, step + 2);
                        serial_serial += test_element;
                    }
                    MegaLog({ data: "start looking to gem_writer_logs table. Reverse serial number order : " + serial_serial });

                }


                var paramsForTagWriterLogs = {
                    TableName: "gem_writer_logs",
                    IndexName: "original_gem_id-index",
                    KeyConditionExpression: "#original_gem_id = :original_gem_id",
                    ExpressionAttributeNames: {
                        "#original_gem_id": "original_gem_id"
                    },
                    ExpressionAttributeValues: {
                        ":original_gem_id": serial_serial
                    }
                };
                const dataForGemWriterLogs = await docClient.query(paramsForTagWriterLogs).promise();
                MegaLog({ channel: "big data", data: dataForGemWriterLogs });

                if (dataForGemWriterLogs.Items.length == 0) {
                    MegaLog({ data: "no tags in gem_writer_logs table" });
                }
                else {
                    MegaLog({ data: "there are " + dataForGemWriterLogs.Items.length + "items in the gem_writer_logs table. " });

                    var firstItem = dataForGemWriterLogs.Items[0];
                    var tagID = firstItem["gem_id"];

                    MegaLog({ data: "firstItem" });
                    MegaLog({ data: firstItem });

                    MegaLog({ data: "gem_id" });
                    MegaLog({ data: tagID });

                    return { tag_id: tagID, tag_source: "gem_writer_logs", tag_id_is_new: false };
                }
            }

            //if we are here that means that nothing was found
            MegaLog({ data: "no tags in any table" });

            var counter = 0;
            for (; counter < 10; counter++) {
                var newTagID = makeRandomId(10);
                var success = await activatedTagExists(newTagID);
                if (success === true) {
                    //TODO log counter and if it is not 0 then we are in deep shit.
                    return { tag_id: newTagID, tag_source: "new", tag_id_is_new: true };

                }
            }

            //if we are here that means something is terribly wrong with the world
            MegaLogError({ error: "Could not generate a correct ID", params, type: "Please try again!" });
            return null;

        }
        else {
            MegaLog({ comments: "there are " + data.Items.length + "items in the activated_tags table" }); // that were activated by iOS

            var firstItem = data.Items[0];
            var tagID = firstItem["tag_id"];

            MegaLog({ data: "firstItem" });
            MegaLog({ data: firstItem });

            MegaLog({ data: "tagID" });
            MegaLog({ data: tagID });

            return { tag_id: tagID, tag_source: "activated_tags", tag_id_is_new: false };

        }

    }
    catch (error) {
        MegaLogError({ error, params, type: "Please try again!", comments: "Database error for getting activated tags list" });
        return null;
    }

    //we returned error previously. But no more!
    //MegaLogError({ type: "Please try again!", comments: "There is already a tag with this serial number in the database. Writing aborted!" });
    //return null;
}

//8
async function activateNewTag({ tag_serial_number, tag_id, user_id, tag_type } = {}) {

    var app_short_info = GetAppShortInfo(); // thats was always nfcunited before
    var app_name = GetAppName();
    //  var bible_action = false;

    // var gem_id = event.gem_id;
    // var gem_name = event.gem_name;

    //gem_name = "Welcome to Galatea!";
    //  var original_gem_id = event.original_gem_id;

    var creation_date = new Date().getTime();
    var creation_date_human = new Date().toISOString();

    // var license_key = "pre_license";

    // if (event.license_key !== undefined) {
    //     license_key = event.license_key;
    //     console.log("license_key:" + license_key);
    // }
    // else {
    //     console.log("license key is not present, we are using pre-license version");
    // }

    // var app_type = "galatea";

    // if (event.appType !== undefined) {
    //     app_type = event.appType;
    //     console.log("app_type:" + app_type);
    // }
    // else {
    //     console.log("app_type is not present, we are using pre-license version(galatea)");
    // }
    var system_user_name = GetSystemUserName(); // GALATEA or MOMENTO or NFCUNITED
    var gem_default_name = GetNewTagName(); // "My new NFC tag" or  "My new gem"


    var new_record = GenerateDefaultRecordInfo();

    // if (typeof event.operation == 'undefined' || event.operation === 'regular') {
    //     //change nothing
    // }
    // else if (event.operation === 'bible') {
    //     new_record = {
    //         "type": "bible",
    //         "public": true,
    //         "text": "bible",
    //         "id": "bible", //was "bibl"!!! error
    //         "date": creation_date
    //     };
    // }
    // else if (event.operation === 'link') {

    //     var link = event.link;
    //     var link_title = event.link_title;

    //     if (typeof link_title == 'undefined') {

    //         if (app_type == "nfcunited") {
    //             link_title = "Welcome!"; //ERROR
    //         }
    //         else {
    //             link_title = "Welcome to Galatea"; //ERROR
    //         }
    //     }
    //     // var id = crypto.randomBytes(20).toString('hex');

    //     new_record = {
    //         "date": creation_date,
    //         "id": "ad_link",
    //         "lastModified": creation_date_human,
    //         "public": true,
    //         "text": link,
    //         "title": "Galatea Link",
    //         "type": "link"
    //     };

    //     if (app_type == "nfcunited") {
    //         new_record["title"] = gem_name;
    //     }
    // }
    const my_email_request = await getUserEmailFromId(user_id);
    var my_email = my_email_request.body;

    var activateNewTagParams = {
        RequestItems: {
            "gems": [{
                PutRequest: {
                    Item: {
                        "gem_id": tag_id,
                        "is_locked": false,
                        "display_name": gem_default_name,
                        "original_gem_id": tag_serial_number,
                        "active_user_id": system_user_name,
                        "hardware_locked": false,
                        "app_name": app_name,

                    }
                }
            }],
            "private_gems": [{

                PutRequest: {
                    Item: {
                        "gem_id": tag_id,
                        "user_id": system_user_name,
                        "gem_name": gem_default_name,
                        "original_gem_id": tag_serial_number,
                        "last_date": creation_date,
                        "items": [new_record],
                    }
                }
            }],
            "activated_tags": [{

                PutRequest: {
                    Item: {
                        "tag_id": tag_id,
                        //"hardware_locked": false,
                        "display_name": gem_default_name,
                        //"original_gem_id": original_gem_id,
                        "tag_serial_number": tag_serial_number,
                        "creation_date": creation_date,
                        "creation_date_human": creation_date_human,
                        "tag_type": tag_type,
                        "activate_user_id": user_id,
                        "activate_email": my_email,
                        "app_name": app_name,
                        "app_short_info": app_short_info,




                    }
                }
            }]
        }
    };


    MegaLog({ data: 'Activating new tag params:' });
    MegaLog({ data: activateNewTagParams });
    try {
        const data = await docClient.batchWrite(activateNewTagParams).promise();
        MegaLog({ data: "activateNewTag succeeded:" });
        return;
    }
    catch (error) {
        MegaLog({ data: error });

        MegaLogError({ type: "ERROR_DB_PROBLEM", error: error, comments: "Unable to update activateNewTag" });
        return;
    }


}

//9
async function lockUnlockGem({ previous_owner, gem_id } = {}) {

    var gem_data_item = await getGem(gem_id);
    if (NoErrors() == true) {
        if (gem_data_item.active_user_id == previous_owner) {

            var is_locked = !gem_data_item.is_locked;

            var params = {
                Key: {
                    "gem_id": gem_id
                },
                TableName: DB_GEMS,
                UpdateExpression: 'set is_locked=:is_locked',
                ExpressionAttributeValues: {
                    ':is_locked': is_locked
                },
                ReturnValues: 'ALL_NEW' //UPDATED_NEW
            };
            MegaLog({ data: 'Changing locked status for gem:' + gem_id + ' for owner:' + previous_owner });
            try {
                const data = await docClient.update(params).promise();
                MegaLog({ data: "lockUnlockGem succeeded:" });
                return;
            }
            catch (error) {
                MegaLogError({ type: "ERROR_DB_PROBLEM", error: error, comments: "Unable to change lock status for gem" });
                return;
            }

        }
        else {
            MegaLogError({ type: ERROR_GEM_OWNER_PROBLEM, comments: "Unable to change lock status for gem because we don't own this gem" });
            return;
        }

    }

}

//10
async function renameMyGem(my_user_id, my_gem_id, gem_new_name) {

    var params = {
        Key: {
            "gem_id": my_gem_id,
            "user_id": my_user_id
        },
        TableName: "private_gems",
        UpdateExpression: 'set gem_name=:gem_name',
        ExpressionAttributeValues: {
            ':gem_name': gem_new_name,
        },
        ReturnValues: 'ALL_NEW' //UPDATED_NEW
    };

    try {
        const data = await docClient.update(params).promise();
        //MegaLog({data:data});
        return;
    }
    catch (error) {
        MegaLogError({ error: error, params: params, comments: "DB error" });
        return;
    }
    //service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err);
    //getMyGemList();


}


async function cleanGem({ my_user_id, gem_id, has_bible } = {}) {


    //2) based on the private gem data we are determining if this is a bible pearl. and if it is - we are writing bible pearl record also as the second record for the system user

    //3)//creating new record for system user
    var new_record = GenerateDefaultRecordInfo();
    var default_items = [new_record];


    var creation_date = new Date().getTime();
    var bible_record = {
        "type": "bible",
        "public": true,
        "text": "bible",
        "id": "bible",
        "date": creation_date
    };

    var default_items = [];
    if (has_bible) { //if this is a bible pearl - then we should add bible record as te first item
        default_items.push(bible_record);
    }

    default_items.push(new_record);

    var system_user_name = GetSystemUserName(); // GALATEA or MOMENTO or NFCUNITED
    var write_items_result = await WriteItemsToPrivateGem({ gem_id: gem_id, user_id: system_user_name, new_items: default_items });
    if (NoErrors() == true) { //4)if new record for system user created - then we are setting system user as the owner of the gem
        await ChangeOwnerForGem({ new_owner: system_user_name, gem_id: gem_id });
        if (NoErrors() == true) { //5)if ownership change is successfull, then we are deleting the old private gem record for the existing user
            await DeletePrivateGem({ gem_id: gem_id, user_id: my_user_id });
            //6)and we are done. 
            return;
        }
    }



}

//11
async function deleteGem({ my_user_id, gem_id } = {}) {

    var private_gem_data = await GetPrivateGem({ gem_id: gem_id, user_id: my_user_id });
    // 1) we check if the gem actually exists

    var has_bible = false;

    if (typeof private_gem_data !== 'undefined') {
        for (var i = 0; i < private_gem_data.items.length; i++) {
            if (private_gem_data.items[i]['type'] == "bible") {
                has_bible = true;
            }
        }
    }


    var am_i_the_owner = await amITheGemOwner(my_user_id, gem_id);

    if (NoErrors() == true) {

        // 2) next we are getting sharing info 
        var gem_sharing_role = await SharingRoleForGem({ my_user_id: my_user_id, gem_id: gem_id });
        if (NoErrors() == true) {

            if (gem_sharing_role.gem_shared) { //gem is shared
                if (gem_sharing_role.share_master) {
                    MegaLog({ channel: DESCRIPTIONS, data: "Gem IS shared and I AM  the owner. Stopping sharing and making a copy to the slave user. Setting up system user data. Changing ownership to system user. Removing my copy of the gem." });

                    await StopSharingGem({ gem_id: gem_id, create_copy_for_slave: true });
                    if (NoErrors() == true) {
                        return await cleanGem({ my_user_id: my_user_id, gem_id: gem_id, has_bible: has_bible });

                    }

                }
                else if (gem_sharing_role.share_slave) {
                    MegaLog({ channel: DESCRIPTIONS, data: "Gem IS shared and I am NOT  the owner. Stopping sharing.  Removing my copy of the gem." });
                    return await StopSharingGem({ gem_id: gem_id, create_copy_for_slave: false });
                }
                else {
                    MegaLog({ channel: DESCRIPTIONS, data: "Gem IS shared but I am NOT involved. Removing my copy of the gem. " });
                    return await DeletePrivateGem({ gem_id: gem_id, user_id: my_user_id });


                }
            }
            else { // gem is NOT shared

                if (am_i_the_owner) { // gem is NOT shared and I AM  the owner
                    MegaLog({ channel: DESCRIPTIONS, data: "Gem is NOT shared and I AM  the owner.  Setting up system user data. Changing ownership to system user. Removing my copy of the gem." });
                    return await cleanGem({ my_user_id: my_user_id, gem_id: gem_id, has_bible: has_bible });
                }
                else {
                    MegaLog({ channel: DESCRIPTIONS, data: "Gem is NOT shared and I am NOT  the owner. Removing my copy of the gem." });
                    return await DeletePrivateGem({ gem_id: gem_id, user_id: my_user_id });

                }


            }

        }
    }


}

//12
async function copyMyGem(my_user_id, fisrt_gem_id, second_gem_id) {

    //TODO create unique identifyers for every record
    //TODO remove private records, do not copy them
    //TODO check if the destination gem is locked
    //TODO set default name


    var private_gem_data = await GetPrivateGem({ gem_id: fisrt_gem_id, user_id: my_user_id });
    if (NoErrors() == true) {

        var firstGemItems = private_gem_data.items;
        var source_gem_name = private_gem_data.gem_name;

        var system_user_name = GetSystemUserName();

        await ChangeOwnerForGem({ new_owner: system_user_name, gem_id: second_gem_id });
        if (NoErrors() == true) {


            var nowDate = new Date().getTime();



            var writeTextParamsToOurGem = {
                Key: {
                    "gem_id": second_gem_id,
                    "user_id": system_user_name // GALATEA or NFCUNITED or MOMENTO
                },
                TableName: DB_PRIAVATE_GEMS,
                UpdateExpression: 'set copy_type = :user , last_date = :last_date , gem_name = :newName  , #items = :items_list ', //gem_name = if_not_exists(#gamName,:newName)
                ExpressionAttributeNames: {
                    '#items': 'items' //, //because its a reservered attribute
                    // '#gamName': 'gem_name'
                },
                ExpressionAttributeValues: {
                    ':items_list': firstGemItems, //[]
                    ':newName': source_gem_name,
                    ':last_date': nowDate,
                    ':user': "user", //that means that we copyed the gem manually and it is not in the 'default' state

                },
                ReturnValues: 'ALL_NEW' //UPDATED_NEW //NONE
            };

            MegaLog({ data: 'copyMyGem gem:' + fisrt_gem_id + ' to :' + second_gem_id });
            try {
                const data = await docClient.update(writeTextParamsToOurGem).promise();
                MegaLog({ data: "copyMyGem succeeded:" });
                return;
            }
            catch (error) {
                MegaLogError({ type: "ERROR_DB_PROBLEM", error: error, comments: "Unable to update item.ChangeOwnerForGem" });
                return;
            }

        }
    }

}
////////////
////INTERNAL
////////////

function makeRandomId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}



async function activatedTagExists(newTagID) {

    var params = {
        TableName: "activated_tags",
        Key: { "tag_id": newTagID }
    };

    var params2 = {
        TableName: "gem_writer_logs",
        Key: { "gem_id": newTagID }
    };

    try {
        const data = await docClient.get(params).promise();
        if (data.Item) {
            MegaLog({ channel: "big data", data: data.Item });
            MegaLog({ data: "Got gem data in activated_tags. Not possible to use tag with ID:" + newTagID }); //error
            return false;
        }

        MegaLog({ data: "no tags with that ID:" + newTagID + " was found at activated_tags." });

        const data2 = await docClient.get(params2).promise();
        if (data2.Item) {
            MegaLog({ channel: "big data", data: data2.Item });
            MegaLog({ data: "Got gem data in gem_writer_logs. Not possible to use tag with ID:" + newTagID }); //error
            return false;
        }
        MegaLog({ data: "no tags with that ID:" + newTagID + " was found at gem_writer_logs." });

        MegaLog({ data: "The ID:" + newTagID + " is good to go" });

        return true;

    }
    catch (error) {
        MegaLogError({ error: error, comments: "DB error" });
        return false;
    }

}
