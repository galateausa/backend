var crypto = require('crypto');

var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });


Object.defineProperty(global, 'DB_PRIAVATE_GEMS', {
    get: function() {
        return "private_gems";
    }
});

Object.defineProperty(global, 'DB_GEMS', {
    get: function() {
        return "gems";
    }
});


Object.defineProperty(global, 'UNKNOWN_ERROR', {
    get: function() {
        return "Something went wrong on our server. Please try again or contact support. Thank you.";
    }
});

Object.defineProperty(global, 'ERROR_YOU_DONT_HAVE_GEMS', {
    get: function() {
        return "You don't have any gems.";
    }
});

Object.defineProperty(global, 'ERROR_GEM_IS_LOCKED', {
    get: function() {
        return "Gem is currently locked by its owner.";
    }
});

Object.defineProperty(global, 'USER_FOR_SHARING_NOT_FOUND', {
    get: function() {
        return "User does not exist in our system. Please check the email that you entered..";
    }
});

Object.defineProperty(global, 'ERROR_GEM_OWNER_PROBLEM', {
    get: function() {
        return "Something went wrong on our server. Please try again or contact support. Thank you.";
    }
});


//channel for MegaLog that logs data both to descriptions and to extra_logs
Object.defineProperty(global, 'DESCRIPTIONS', {
    get: function() {
        return "descriptions";
    }
});

Object.defineProperty(global, '__stack', {
    get: function() {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__function', {
    get: function() {
        //  var tempArr = [];
        for (var i = 0; i < __stack.length; i++) {
            var funcName = __stack[i].getFunctionName();
            var funcLine = __stack[i].getLineNumber();

            if (funcName === null) {
                return " ";
            }
            else {
                if (!funcName.includes('egaLog') && funcName !== "get" && funcName !== "exports.handler" && funcName !== "handleOnce") {
                    var funcNameCombinedString = funcName + ':' + funcLine;
                    var calcAmmountOfSpaces = Math.max(0, 44 - funcNameCombinedString.length);
                    for (var i = 0; i < calcAmmountOfSpaces; i++) {
                        funcNameCombinedString = funcNameCombinedString + " ";
                    }
                    return funcNameCombinedString;
                }
            }
        }
        return " ";
        // return tempArr;
    }
});

/*global __stack*/
/*global __line*/
/*global __function*/

require('./dynamo.js')();


require('./user_connection.js')();
/*global GetMyGems*/
/*global GetUserIdFromEmail*/
/*global ConnectUser*/

require('./pusher.js')();
/*global SendPushNotofication*/

require('./sharing.js')();
/*global GetListOfActiveSharingRequests*/
/*global GetListOfActiveSharingRequestsForOwner*/
/*global GetUserIDForActionOnGem*/
/*global GetListOfSharingRequests*/
/*global IsSharingActiveForMeAndThisGem*/
/*global GetSharingRequestByID*/
/*global RemoveSharingRequest*/
/*global SharingRoleForGem*/
/*global StopSharingGem*/

require('./private_gems.js')();
/*global GetPrivateGem*/
/*global WriteItemsToPrivateGem*/
/*global DeletePrivateGem*/

require('./mega_logger.js')();
/*global  InitMegaLog*/
/*global  RunMegaLog*/
/*global MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/
/*global GetTotalExcecutionTime*/
/*global GetFinalErrorType*/
/*global GetSystemUserName*/
/*global GenerateDefaultRecordInfo*/


require('./gems.js')();
/*global  GetGem*/
/*global GetRealGemID*/
/*global AmITheGemOwner*/
/*global GetGemOwnerID*/
/*global ChangeOwnerForGem*/
/*global LockUnlockGem*/
/*global  RenameMyGem*/
/*global DeleteGem*/
/*global CopyMyGem*/

require('./record_operations.js')();
/*global CopyMyRecord*/
/*global MoveMyRecord*/
/*global  RenameMyRecord*/
/*global DeleteMyRecord*/
/*global WriteMyRecord*/
/*global AddRecordToPrivateGem*/
/*global MakeMyRecordPublic*/
/*global GiftMyRecord*/

/*global ERROR_GEM_IS_LOCKED*/


exports.handler = async(event, context) => {

    //  console.warn("not desktop");
    //  console.warn(typeof context);
    //  console.warn(context);
     // console.warn(JSON.stringify(context, null, 4));

    //  console.warn(Object.keys(context).length)
    //  console.warn(context.identity )

    var desktop_access_password = event.desktop_access_password;
    if (typeof context.identity === 'undefined' && typeof desktop_access_password !== 'undefined' && desktop_access_password === "GIDI^gv4CDgdG$g5ggcdggv45g4d5GVD$") {
        console.log("desktop");
        var desktop_user_id = event.desktop_user_id;
        var new_identity = {
            cognitoIdentityId: desktop_user_id,
            // last_name: "CSS",
            // age: 32,
            //  website: "java2s.com"
        };



        context.identity = new_identity

    }

    if (typeof context.clientContext === 'undefined' && typeof desktop_access_password !== 'undefined' && desktop_access_password === "GIDI^gv4CDgdG$g5ggcdggv45g4d5GVD$") {

        var new_client_context = {
            client: {
                app_version_name: "1.0",
                app_title: "MomentoDesktop",
                app_package_name: "com.momentonfc.desktop"
            },
            env: {
                platform: "1.0",
                platform_version: "Windows",
                model_version: "Desktop"
            }
        };
        context.clientContext = new_client_context;
    }


    function isPublic(value) { //for filtering
        return value.public == true;
    }

    await InitMegaLog(context, event);
    var my_user_id = context.identity.cognitoIdentityId; //const

    const operation = event.operation;
    var result;

    switch (operation) {

        case 'get_list_of_shared_gems':
            const not_owner_result = await GetListOfActiveSharingRequests(my_user_id); // where I am not the owner
            const owner_result = await GetListOfActiveSharingRequestsForOwner(my_user_id); // where I am the owner

            // console.log(pending_sharing_requests_result);
            // if(active_sharing_requests_result.error == false && active_sharing_requests_for_owner_result.error == false){

            // var result3 = [];
            // if (Array.isArray(active_sharing_requests_result) == true) {
            //     //   console.log("lenght =" + result1.length);

            //     for (var i = 0; i < active_sharing_requests_result.length; i++) {
            //         var elmnt = active_sharing_requests_result[i];
            //         var gem_id_from_element = elmnt.gem_id;
            //         var owner_user_id_element = elmnt.owner_user_id;
            //         //         console.log(owner_user_id_element, gem_id_from_element);

            //         var private_elmnt = await GetPrivateGem(gem_id_from_element, owner_user_id_element);

            //         result3.push(private_elmnt);
            //         // console.log( private_elmnt.body);
            //     }
            // }
            var owner_result_id_and_names_only = owner_result.map(function(itm) {
                return { gem_name: itm.gem_name, gem_id: itm.gem_id, owner: true, shared_with: itm.user_email };
            });

            var not_owner_result_id_and_names_only = not_owner_result.map(function(itm) {
                return { gem_name: itm.gem_name, gem_id: itm.gem_id, owner: false, shared_with: itm.owner_user_email };
            });
            var result = { "owner": owner_result_id_and_names_only, "not_owner": not_owner_result_id_and_names_only }; //, "not_owner_items": result3
            // }
            break;

        case 'get_pending_sharing_requests':
            const pending_sharing_requests_result = await GetListOfSharingRequests(my_user_id);
            if (NoErrors() == true) {
                result = pending_sharing_requests_result;
            }
            break;

        case 'turn_sharing_notifications':
            var sharing_request_id = event.sharing_request_id;
            var sharing_notification_sound_on_or_off = event.sharing_notification_sound_on_or_off;
            var sharingRequestInfo = await GetSharingRequestByID(sharing_request_id);

            //console.log(sharingRequestInfo);
            if (NoErrors() == true) {
                const user_request_id = sharingRequestInfo.user_id;
                const gem_request_id = sharingRequestInfo.gem_id;

                const acceptSharingRequestTask = await TurnSharingNotification(user_request_id, gem_request_id, sharing_notification_sound_on_or_off);

                //var result = pending_sharing_requests_result.body;
            }
            break;


        case 'accept_sharing_request':
            var sharing_request_id = event.sharing_request_id;
            var sharingRequestInfo = await GetSharingRequestByID(sharing_request_id);

            //console.log(sharingRequestInfo);
            if (NoErrors() == true) {

                const user_request_id = sharingRequestInfo.user_id;
                const gem_request_id = sharingRequestInfo.gem_id;

                const acceptSharingRequestTask = await AcceptSharingRequest(user_request_id, gem_request_id);

                console.log(JSON.stringify(sharingRequestInfo, null, 2));

                //const sharing_request_by_id_result = await GetSharingRequestByID(sharing_request_body);
                // MegaLog(["test:",sharing_request_by_id_result]);
                const my_email_request = await getUserEmailFromId(my_user_id);
                var my_email = my_email_request.body;
                var messageTitle = "Success!";
                var messageMessage = "User '" + my_email + "' accepted your sharing request! You are now sharing the tag together.";

                var pusheUserDestinationID = sharingRequestInfo.owner_user_id; // we need to sent the message to the owner of the gem. so glad that we have this ID already
                // if (user_action_id === my_user_id) {
                //     pusheUserDestinationID = await GetSlaveUserForGem(my_user_id, gem_id);
                // }
                //send sharing_request_id for helping
                MegaLog(["pusheUserDestinationID:" + pusheUserDestinationID, messageTitle, messageMessage]);
                await SendPushNotofication(pusheUserDestinationID, messageTitle, messageMessage, "sharing_accepted"); //sharing_denied



                //var result = pending_sharing_requests_result.body;
            }
            break;

        case 'deny_sharing_request':
            var sharing_request_id = event.sharing_request_id;
            var sharingRequestInfo = await GetSharingRequestByID(sharing_request_id);

            //console.log(sharingRequestInfo);

            if (NoErrors() == true) {

                const user_request_id = sharingRequestInfo.user_id;
                const gem_request_id = sharingRequestInfo.gem_id;
                const denySharingRequestTask = await RemoveSharingRequest(user_request_id, gem_request_id);

                //var result = pending_sharing_requests_result.body;
            }
            break;

        case 'stop_sharing':
            var sharing_request_id = event.sharing_request_id;

            const stop_sharing_result = await StopSharingGem({ sharing_request_id: sharing_request_id });

            if (NoErrors() == true) {
                var my_gems = await GetMyGems({ my_user_id });
                if (NoErrors() == true) {
                    result = my_gems;
                }
            }
            break;

        case 'start_sharing_gem_with_user':
            //
            // can only share the gem if you are the owner!!!
            //
            var other_user_email = event.other_user_email;
            other_user_email = other_user_email.toLowerCase();
            var gem_id = event.gem_id;

            const my_email_request = await getUserEmailFromId(my_user_id);
            if (NoErrors() == true) {
                const other_user_id_request_result = await GetUserIdFromEmail({ email: other_user_email });
                if (NoErrors() == true) {
                    const private_gem_request = await GetPrivateGem({ gem_id: gem_id, user_id: my_user_id });

                    if (NoErrors() == true && my_email_request.error == false) {

                        const my_email = my_email_request.body;
                        MegaLog('got my email:' + my_email);

                        const other_user_id = other_user_id_request_result;
                        MegaLog('got other_user_id:' + other_user_id);

                        const current_gem_name = private_gem_request.gem_name;
                        MegaLog('got gem  name:' + current_gem_name);

                        const sharing_request_put_status = await AddSharingRequest(my_email, my_user_id, other_user_email, other_user_id, gem_id, current_gem_name)

                        if (sharing_request_put_status.error == false) {

                            const messageMessage = 'User ' + my_email + " would like to share '" + current_gem_name + "' with you. Tap here to ACCEPT.";
                            const messageTitle = "New sharing request from '" + my_email + "'";
                            MegaLog(messageMessage + " " + messageTitle);
                            await SendPushNotofication(other_user_id, messageTitle, messageMessage);

                            //returning my gems list
                            var my_gems = await GetMyGems({ my_user_id });
                            if (NoErrors() == true) {
                                result = my_gems;
                            }
                        }
                        else {
                            MegaLog("sharing request creation failed, db error");

                            //console.log();
                        }


                    }
                    else {
                        if (my_email_request.error != false) {
                            //console.log("Could not get my email, it is not present in the DB or his account is not activated");
                        }

                        /// if (private_gem_request_result.error != false) {
                        //     //console.log("Could not get gem details");
                        // }


                    }
                }
                else {
                    //console.log("Could not get other user id, his email is not present in the DB or his account is not activated");
                }
            }
            else {
                //could not get my email from id. this is super weird and should not happen.
            }
            break;

        case "rename_my_gem":

            var gem_id = event.gem_id;
            var gem_new_name = event.gem_name;

            var user_action_id = await GetUserIDForActionOnGem(my_user_id, gem_id);
            if (NoErrors() == true) {
                const rename_my_gem_result = await RenameMyGem(user_action_id, gem_id, gem_new_name);

                var my_gems = await GetMyGems({ my_user_id });
                if (NoErrors() == true) {
                    result = my_gems;
                }

            }
            //get_my_gems and return them
            break;

        case "request_new_gem_id":
            var tag_serial_number = event.tag_serial_number;


            var tag_data = await GetNewTag(tag_serial_number);
            var tag_id = tag_data["tag_id"];
            var tag_source = tag_data["tag_source"];
            var tag_id_is_new = tag_data["tag_id_is_new"];

            MegaLog({ data: tag_data });

            if (NoErrors() == true) {
                if (tag_id === null || tag_id === undefined) {
                    //TODO log error

                }
                var tag_type = event.tag_type;

                MegaLog({ data: "tag_id=" + tag_id });
                if (tag_id_is_new == true) {
                    await ActivateNewTag({ tag_serial_number: tag_serial_number, tag_id: tag_id, user_id: my_user_id, tag_type: tag_type });
                }

                if (NoErrors() == true) {
                    result = { tag_id: tag_id };
                }
            }
            break;

        case "rename_my_record":

            var gem_id = event.gem_id;
            var new_record_name = event.new_record_name;
            var record_id = event.record_id;

            var user_action_id = await GetUserIDForActionOnGem(my_user_id, gem_id);

            //getting sharing info 
            var gem_sharing_role = await SharingRoleForGem({ my_user_id: my_user_id, gem_id: gem_id });


            if (NoErrors() == true) {

                const rename_my_record_result = await RenameMyRecord(user_action_id, gem_id, record_id, new_record_name);
                if (gem_sharing_role.gem_shared == true) { //

                    const my_email_request = await getUserEmailFromId(my_user_id);
                    var my_email = my_email_request.body;
                    var messageMessage = "You have a new message from '" + my_email + "'.Tap here to open!";
                    var pusheUserDestinationID = user_action_id; // we need to sent the message to the owner of the gem. so glad that we have this ID already
                    if (user_action_id === my_user_id) {
                        pusheUserDestinationID = await GetSlaveUserForGem(my_user_id, gem_id);
                    }
                    MegaLog({ data: [messageMessage, "pusheUserDestinationID:" + pusheUserDestinationID] });
                    var messageTitle = "New sharing request from '" + my_email + "'";

                    await SendPushNotofication(pusheUserDestinationID, messageTitle, messageMessage);
                }
                if (NoErrors() == true) {
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }
                }


            }
            break;

            //  function getGemRealId(gem_id_from_app, callback) {
            //         if (gem_id_from_app.startsWith("OLD:")) { // gem is from the previous anroid version.

            //             var msgID = gem_id_from_app.replace('OLD:', '');
            //             var requestParams = { Key: { "message_id": msgID }, TableName: constants.DB_GEMS_CONNECTION };

            //             dynamoRequest.getItem(requestParams, function(err, data) {
            //                 if (err) {
            //                     service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err);
            //                 }
            //                 else {
            //                     if (data.Item && data.Item.gem_id) {
            //                         var gemid = data.Item.gem_id;
            //                         megaLogger["extra_logs"].push('calculated real id:', gemid, ' from message:', gem_id_from_app);
            //                         //megaLogger["gem_id"] = gemid;
            //                         callback(gemid);
            //                     }
            //                     else {
            //                         service.SendBackError(megaLogger, constants.ERROR_GEM_NOT_FOUND);
            //                     }
            //                 }
            //             });
            //         }
            //         else { // gem is from gem writer
            //             //megaLogger["extra_logs"].push('got real gem id from the beginning:', gem_id_from_app);
            //             // megaLogger["gem_id"] = gem_id_from_app;
            //             callback(gem_id_from_app);
            //         }
            //     }


        case 'gem_latest_read':

            var gem_id_from_app = event.gem_id;
            var real_gem_id = await GetRealGemID(gem_id_from_app);

            var private_gem_data = await GetPrivateGem({ gem_id: real_gem_id, user_id: my_user_id });
            if (NoErrors() == true) {
                var gem_items = private_gem_data.items;
                if (gem_items && gem_items.length) {
                    var filtered_items_forced = [];
                    filtered_items_forced.push(gem_items[0]);
                    result = filtered_items_forced;
                }
                else {
                    //send error
                }
            }

            break;


        case "gem_public_read":

            var gem_id_from_app = event.gem_id;
            var real_gem_id = await GetRealGemID(gem_id_from_app);

            //  var gem_id_from_app = event.gem_id;
            // getGemRealId(gem_id_from_app, function(real_gem_id) {
            //   gemPublicRead(real_gem_id);
            //megaLogger["extra_logs"].push('gemPublicRead.1.Request params for gem_public_read from  gems table:', JSON.stringify(thisPullParams, null, 2));
            //                    service.SendBackError(megaLogger, constants.ERROR_GEM_NOT_FOUND);

            var gemDataaa = await GetGem(real_gem_id);
            if (NoErrors() == true) {
                var active_user_id = gemDataaa.active_user_id;
                if (!active_user_id) { //this means that noone wrote to this gem yet and it is empty
                    //  megaLogger["extra_logs"].push('gemPublicRead.1.1the records are empty for the item item:', JSON.stringify(data_gems.Item, null, 2));
                    //  service.SendBackError(megaLogger, constants.ERROR_GEM_EMPTY);
                    //SET ERROR

                }
                else {

                    // megaLogger["extra_logs"].push('gemPublicRead.2.Request params for gem_public_read from private gems table:', JSON.stringify(requestParams, null, 2));

                    var private_gem_data = await GetPrivateGem({ gem_id: real_gem_id, user_id: active_user_id });
                    if (NoErrors() == true) {

                        var gem_items = private_gem_data.items;
                        if (gem_items && gem_items.length) {

                            var owner = "me";
                            if (active_user_id !== my_user_id) {
                                owner = "notme";
                            }

                            var filtered_items = gem_items.filter(isPublic);
                            var filtered_items_count = Object.keys(filtered_items).length;
                            if (filtered_items_count) {
                                //TODO log error
                                result = { "items": filtered_items, "owner": owner };
                                // service.SendBackResult(megaLogger, constants.SUCCESS_PUBLIC_ITEMS_FOUND, filtered_items, owner);
                            }
                            else {

                                var filtered_items_forced = [];
                                filtered_items_forced.push(gem_items[0]);
                                result = { "items": filtered_items_forced, "owner": owner };

                                // service.SendBackResult(megaLogger, constants.SUCCESS_PUBLIC_ITEMS_FOUND, filtered_items_forced, owner);
                            }
                        }
                        else {
                            //TODO LOG ERROR
                            result = {} //{filtered_items_forced,owner}
                            //service.SendBackError(megaLogger, constants.ERROR_GEM_NO_ITEMS, err);
                        }
                    }
                    else {
                        //                                service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err);
                        //                                    service.SendBackError(megaLogger, constants.ERROR_GEM_EMPTY, err);

                    }





                }
            }
            else {
                //                service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err_gems);

            }




            break;
        case "delete_record":
            var gem_id = event.gem_id;
            var record_id = event.record_id;

            var user_action_id = await GetUserIDForActionOnGem(my_user_id, gem_id);
            if (NoErrors() == true) {
                const delete_my_record_result = await DeleteMyRecord(user_action_id, gem_id, record_id);
                if (NoErrors() == true) {
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }
                }
            }
            break;

        case "delete_gem":
            var gem_id = event.gem_id;

            const delete_gem_result = await DeleteGem({ my_user_id: my_user_id, gem_id: gem_id });
            MegaLog({ data: "done deleting gem, will ask for MyGem  list now" });
            if (NoErrors() == true) {
                var my_gems = await GetMyGems({ my_user_id });
                if (NoErrors() == true) {
                    result = my_gems;
                }
            }

            break;

        case "make_record_public":
            var gem_id = event.gem_id;
            var record_id = event.record_id;

            var user_action_id = await GetUserIDForActionOnGem(my_user_id, gem_id);
            if (NoErrors() == true) {
                const delete_my_record_result = await MakeMyRecordPublic(user_action_id, gem_id, record_id);
                if (NoErrors() == true) {
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }
                }
            }
            break;

        case 'gem_write_text_record':
        case 'gem_write_link_record':
        case 'gem_write_sound_record':
        case 'gem_write_video_record':
        case 'gem_write_image_record':
        case 'gem_write_pdf_record':

            var gem_id_from_app = event.gem_id;
            var real_gem_id = await GetRealGemID(gem_id_from_app);

            var record_type = operation.replace("gem_write_", "");
            record_type = record_type.replace("_record", "");

            var text = event.text;
            var titleText = event.title_text;

            var file_var = event.file;

            //var sharing_requests_data = await GetListOfActiveSheringRequestsForGem(real_gem_id);

            //getting gem details
            var gem_data = await GetGem(real_gem_id);

            //getting sharing info 
            var gem_sharing_role = await SharingRoleForGem({ my_user_id: my_user_id, gem_id: real_gem_id });

            //getting owner
            var gem_owner_id = await GetGemOwnerID(real_gem_id, gem_data);


            if (NoErrors() == true) {

                //var do_we_share_this_gem = await IsSharingActiveForMeAndThisGem(my_user_id, real_gem_id);
                //var do_we_share_this_gem2 = await GetSlaveUserForGem(my_user_id, real_gem_id);

                if (gem_sharing_role.i_am_involved_in_sharing == true) {

                    MegaLog({ data: "I am somehow involed in sharing. No need to change ownership. Saving record to the gems REAL owner(maybe not me)." });

                    await AddRecordToPrivateGem({ type: record_type, text: text, title: titleText, gem_id: real_gem_id, gem_owner_id: gem_owner_id, file: file_var });

                    //sending notification. TODO - CHECK IF NOTIFICATIONS ARE INDEED NEEDED
                    const my_email_request = await getUserEmailFromId(my_user_id);
                    var my_email = my_email_request.body;
                    var messageMessage = "You have a new message from '" + my_email + "'. Tap here to open!";
                    var pusheUserDestinationID = gem_owner_id; // we need to sent the message to the owner of the gem. so glad that we have this ID already
                    if (gem_owner_id == my_user_id) {
                        pusheUserDestinationID = await GetSlaveUserForGem(my_user_id, real_gem_id);
                        //I am the owner
                    }
                    else {
                        // I am not the owner
                    }
                    MegaLog({ data: ["pusheUserDestinationID:" + pusheUserDestinationID, messageMessage] });

                    var push_extra_data = {
                        "push_type": "new_record",
                        "gem_id": real_gem_id
                    };
                    var messageTitle = "New message";
                    await SendPushNotofication(pusheUserDestinationID, messageTitle, messageMessage, push_extra_data);
                    //sending notification TODO - CHECK IF NOTIFICATIONS ARE INDEED NEEDED

                }
                else {
                    MegaLog({ data: "I am not involved in sharing the gem in any way or sharing is off" });
                    if (gem_owner_id == my_user_id) {
                        MegaLog({ data: "I am the owner of the gem. No need to change ownership.  Saving record for my id" });
                        await AddRecordToPrivateGem({ type: record_type, text: text, title: titleText, gem_id: real_gem_id, gem_owner_id: my_user_id, file: file_var });

                    }
                    else {

                        MegaLog({ data: "I am not the owner of the gem." });

                        if (gem_data.is_locked == true) {
                            MegaLog({ data: "Gem is locked. Do nothing" });
                            MegaLogError({ type: ERROR_GEM_IS_LOCKED });
                        }
                        else {
                            MegaLog({ data: "Gem is not locked." });

                            if (gem_sharing_role.gem_shared == true) { //
                                MegaLog({ data: "Sharing is active. we are finishing it (copying to all the users and deleting the request)" });
                                const stop_sharing_result = await StopSharingGem({ gem_id: real_gem_id });
                            }
                            else {
                                MegaLog({ data: "Sharing is NOT active. we are finishing it (copying to all the users and deleting the request)" });

                            }

                            MegaLog({ data: "Need  to  append the data to my private gem. Also need to migrate all the public data from the previous owner" });
                            await AddRecordToPrivateGem({ type: record_type, text: text, title: titleText, gem_id: real_gem_id, gem_owner_id: my_user_id, file: file_var, migrate_from: gem_owner_id });

                        }

                    }
                }
            }

            break;

        case 'my_gem_list':
            var my_gems = await GetMyGems({ my_user_id });
            if (NoErrors() == true) {
                result = my_gems;
            }
            break;
        case 'user_connection':


            // await SendPushNotofication("user_id", "title", "message");

            break;
        case 'lock_gem':

            var gem_id_from_app = event.gem_id;

            var lock_unlock_result = await LockUnlockGem({ previous_owner: my_user_id, gem_id: gem_id_from_app });
            if (NoErrors() == true) {

                //returning my gem list after successfully changing his lock\unlock status
                var my_gems = await GetMyGems({ my_user_id });
                if (NoErrors() == true) {
                    result = my_gems;
                }

            }

            break;


        case 'move_my_record':

            var gem_id = event.gem_id;
            var record_id = event.record_id;

            var record_source_index = event.record_source_index;
            var record_destination_index = event.record_destination_index;

            //var private_gem_data = await GetPrivateGem({ gem_id: gem_id, user_id: my_user_id });  // WHAT IS THAT DOING HERE????!?
            if (NoErrors() == true) {
                const move_my_record_result = await MoveMyRecord(my_user_id, gem_id, record_source_index, record_destination_index);
                if (NoErrors() == true) {
                    //returning my gem list after successfully changing his lock\unlock status
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }

                }

            }

            break;

        case 'copy_my_record':

            //there can be 9 options when copying records:

            //copy from my private gem  to my another private gem 
            //copy from my private gem  to another shared gem (where I am the owner )
            //copy from my private gem  to another shared gem (where I am NOT the owner )

            //copy from shared gem (where I am the owner ) to my another private gem 
            //copy from shared gem (where I am the owner ) to another shared gem (where I am the owner )
            //copy from shared gem (where I am the owner ) to another shared gem (where I am NOT the owner )

            //copy from shared gem (where I am NOT the owner ) to my another private gem 
            //copy from shared gem (where I am NOT the owner ) to another shared gem (where I am the owner )
            //copy from shared gem (where I am NOT the owner ) to another shared gem (where I am NOT the owner )

            //its good that for this logic it does not matter if you own your private gem or not. otherwise it would have been 16 scenarios, not 9


            var first_gem_id = event.gem_id;
            var second_gem_id = event.second_gem_id;

            var record_id = event.record_id;

            var record_source_index = event.record_source_index;
            var record_destination_index = event.record_destination_index;


            if (NoErrors() == true) {
                const copy_my_record_result = await CopyMyRecord(my_user_id, first_gem_id, second_gem_id, record_source_index, record_destination_index);
                if (NoErrors() == true) {
                    //returning my gem list after successfully changing his lock\unlock status
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }

                }

            }

            break;

        case 'copy_my_gem':


            //TODO create unique identifyers for every record
            //TODO remove private records, do not copy them
            //TODO check if the destination gem is locked
            //TODO set default name

            var first_gem_id = event.gem_id;
            var second_gem_id = event.second_gem_id;


            if (NoErrors() == true) {
                const copy_my_record_result = await CopyMyGem(my_user_id, first_gem_id, second_gem_id);
                if (NoErrors() == true) {
                    console.log("NoErrors() == true")
                    //returning my gem list after successfully changing his lock\unlock status
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }

                }

            }
            break;
        case 'gift_my_record':


            var first_gem_id = event.gem_id;
            var second_gem_id = event.second_gem_id;

            var record_id = event.record_id;

            if (NoErrors() == true) {
                const gift_my_record_result = await GiftMyRecord(my_user_id, first_gem_id, second_gem_id, record_id);
                if (NoErrors() == true) {
                    //returning my gem list after successfully changing his lock\unlock status
                    var my_gems = await GetMyGems({ my_user_id });
                    if (NoErrors() == true) {
                        result = my_gems;
                    }

                }

            }

            break;

        case 'after_register':

            const my_email__ = event.email;
            const my_sub_id__ = event.sub;
            var tempResult = await ConnectUser(my_email__, my_sub_id__, my_user_id);
            // if (NoErrors() == true) {
            //   }
            break;



        case 'error_log':
            MegaLog({ data: "error_log" });
            MegaLog({ data: event });
            break;


        case 'debug_log':
            MegaLog({ data: "debug_log" });
            MegaLog({ data: event });


        default:
            var result = "unknown request type";
    }

    //megaLogger["extra_logs"].push('Function begin');
    //    var user_id = context.identity.cognitoIdentityId;
    // var reslt =  await getListOfRequestsForUserEmail(user_email);


    var response = {
        status_name: "success",
        status: "success",
        status_code: "0",
        time_total: GetTotalExcecutionTime(),
        items: result
    };

    if (operation === "gem_public_read") {

        if (result && result.items && result.items.length > 0) {
            response.items = result.items;
        }
        if (result && result.owner) {
            response.owner = result.owner;
        }

    }
    else {
        ///
    }


    if (NoErrors() == false) {
        response.status = "error";
        //console.log(GetFinalErrorType());
        response.status_name = GetFinalErrorType();
    }

    MegaLog({ channel: "response", data: response });

    RunMegaLog();
    return response;
};
