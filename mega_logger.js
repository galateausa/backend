var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });


var megaLogObject = new Object();
var errorIsPresent = false;
var time_start = 0;
var debug_mode = true;
var final_error_type = "NO_ERROR";
var logCounter = 0;

require('./dynamo.js')();

/*global DESCRIPTIONS*/

/*global UNKNOWN_ERROR*/


/*global __function*/


// Object.defineProperty(global, '__stack', {
// get: function() {
//         var orig = Error.prepareStackTrace;
//         Error.prepareStackTrace = function(_, stack) {
//             return stack;
//         };
//         var err = new Error;
//         Error.captureStackTrace(err, arguments.callee);
//         var stack = err.stack;
//         Error.prepareStackTrace = orig;
//         return stack;
//     }
// });

// Object.defineProperty(global, '__line', {
// get: function() {
//         return __stack[1].getLineNumber();
//     }
// });

// Object.defineProperty(global, '__function', {
// get: function() {
//         return [__stack[0].getFunctionName(),__stack[1].getFunctionName(),__stack[2].getFunctionName()];
//     }
// });




module.exports = function() {
    this.MegaLog = ({ channel , data, log_line = true, comments = "" } = {}) => {
        return megaLog({ channel, data, log_line, comments });
    };

    this.RunMegaLog = () => {
        return runMegaLog();
    };

    this.GetAppShortInfo = () => {
        return getAppShortInfo();
    }
    this.GetAppName = () => {
        return getAppName();
    }

    this.InitMegaLog = async(context, event) => {
        return initMegaLog(context, event);
    };

    this.MegaLogError = ({ error = "", comments, comment, params = "", type = UNKNOWN_ERROR } = {}) => {
        console.log(type);
        if (typeof comments === 'undefined' && typeof comment !== 'undefined') {
            comments = comment;
        }
        if (typeof comments === 'undefined') {
            comments = ""
        }
        return megaLogError({ error, comments, params, type });
    };

    this.MegaLogDebug = (new_debug_mode) => {
        debug_mode = new_debug_mode
        return;
    }

    this.NoErrors = () => {
        if (errorIsPresent == true) {
            return false;
        }
        else {
            return true;
        }
    };

    this.GetTotalExcecutionTime = () => {
        return getTotalExcecutionTime();
    };

    this.GetFinalErrorType = () => {
        return getFinalErrorType();
    };

    //returns system user that needs to own the gem when we first creating it or deleting it
    this.GetSystemUserName = () => {
        return getSystemUserName();
    }

    // returns new tag name based on the app that requests this
    this.GetNewTagName = () => {
        return getNewTagName();
    }

    // returns dictionary of propertyes for the new default record based on the app type
    this.GenerateDefaultRecordInfo = () => {
        return generateDefaultRecordInfo();
    }



};

function getTotalExcecutionTime() {
    var function_time_end = new Date().getTime();
    var function_time_total = (function_time_end - time_start) / 1000.0;
    return function_time_total;
}

function getFinalErrorType() {
    return final_error_type;
}

function megaLogError({ error, comments, params, type } = {}) {
    megaLog({ data: __function + type });
    if (final_error_type === "NO_ERROR") {
        final_error_type = type; //first error is displayed only
    }
    var funcName = __function;
    var error_object = [funcName, params, error];
    if (comments !== undefined) {
        if (error !== undefined) {
            error_object = [funcName, params, comments, error];
        }
        else {
            error_object = [funcName, params, error];
        }
    }
    else {
        error_object = [funcName, params];
    }
    //megaLog("extra_logs", error_object);
    megaLog({ channel: "errors", data: error_object });
    errorIsPresent = true;
}

function getAppShortInfo() { //return g or n or m
    if (megaLogObject["app"] === undefined) {
        return " ";
    }
    else {
        return megaLogObject["app"]; // app type + app platform + app version
    }

}

function getAppName() { //return galatea or nfcunited or momento
    if (megaLogObject["app_name"] === undefined) {
        //DO NOT DELETE THIS BECAUSE SEVERAL FUNCTUIONS LIKE  getSystemUserName and generateDefaultRecordInfo depend on this default value
        return "galatea"; // this should not be possible TODO log this error
        //DO NOT DELETE THIS BECAUSE SEVERAL FUNCTUIONS LIKE  getSystemUserName and generateDefaultRecordInfo depend on this default value
    }
    else {
        return megaLogObject["app_name"]; // galatea or nfcunited or momento
    }

}

function getSystemUserName() { //returns system user that needs to own the gem when we first creating it or deleting it
    var appName = getAppName();

    if (appName == "galatea") {
        return "GALATEA";

    }
    else if (appName == "nfcunited") {
        return "NFCUNITED";

    }
    else if (appName == "momento") {
        return "MOMENTO";

    }
    else {
        // TODO log this error, this should not happen
        return "GALATEA";
    }

}

function generateDefaultRecordInfo() {

    var app_name = getAppName();

    var creation_date = new Date().getTime();
    var creation_date_human = new Date().toISOString();

    if (app_name == "galatea") {
        return {
            "type": "sound",
            "file": "old/4996_DEFAULT_MESSAGE.aac",
            "public": true,
            "id": "4996",
            "date": creation_date,
            "text": "Galatea Momento Pearl",
            "title": "Welcome to Galatea!",
            "lastModified": creation_date_human,
        };
    }
    else if (app_name == "nfcunited") {
        return {
            "type": "image",
            "file": "system/welcome-nfc-united.png",
            "public": true,
            "id": "4996",
            "date": creation_date,
            "text": "Welcome!",
            "title": "Welcome!",
            "lastModified": creation_date_human,
        };

    }
    else if (app_name == "momento") {
        return {
            "type": "image",
            "file": "system/welcome-momento.png",
            "public": true,
            "id": "4996",
            "date": creation_date,
            "text": "Welcome!",
            "title": "Welcome!",
            "lastModified": creation_date_human,
        };
    }
    else {
        //this should not happen because getAppName always returns "galatea as default"
    }
}

function getNewTagName() { // returns new tag name based on the app that requests this
    var appName = getAppName();
    if (appName == "galatea") {
        return "My new gem";

    }
    else if (appName == "nfcunited") {
        return "My new NFC tag";

    }
    else if (appName == "momento") {
        return "My new NFC tag";

    }
    else {
        // TODO log this error, this should not happen
        return "My new gem";

    }
}

function runMegaLog() {

    if (errorIsPresent == true) {
        megaLogObject["status"] = "error";
    }

    delete megaLogObject["debug:context"];

    if (errorIsPresent == false) {
        delete megaLogObject["debug:event"];
        delete megaLogObject["debug:request"];
        delete megaLogObject["debug:response"];
    }

    megaLog({ channel: "time_total", data: getTotalExcecutionTime(), log_line: false });
    console.log(JSON.stringify(megaLogObject, null, 2));
}

function megaLog({ channel = "extra_logs", data, log_line, comments } = {}) {

logCounter++;
    if (channel === DESCRIPTIONS) { //for descriptions channel we need to log 2 times - so we are calling this fuction recurseively
        megaLog({ data, log_line, comments });
    }

    if (typeof comments === "string" && comments.length > 0) { //always log line when we have comments
        log_line = true;
    }

    var new_data = data;
    if (log_line === true) {
        var funcName = "("+logCounter + ") " + __function;
        if (typeof data === "string" || typeof data === "number") {
            new_data = funcName + data;
            if (comments.length > 0) {
                new_data = funcName + "(" + comments + ") " + data;
            }
            else {
                new_data = funcName + data;
            }
        }
        else {
            if (comments.length > 0) {
                new_data = [funcName + "(" + comments + ") ", data];

            }
            else {
                new_data = [funcName, data];
            }

        }
    }



    //console.log(stream, data);
    if (megaLogObject[channel] === undefined) { // if the stream is empty - then we set it as a normal variable
        if (Array.isArray(new_data) == true) {
            megaLogObject[channel] = [new_data];
        }
        else {
            megaLogObject[channel] = new_data;
        }
    }
    else {
        if (Array.isArray(megaLogObject[channel]) == false) {
            // if the stream is already there but is not an array we are not overwriting it but creating an array and migrating existing data there
            var tempVar = megaLogObject[channel];
            //console.log(megaLog[stream]);
            //var newTempVar = JSON.parse(JSON.stringify(megaLog[stream]));
            megaLogObject[channel] = [];
            megaLogObject[channel].push(tempVar);
        }
        megaLogObject[channel].push(new_data);
    }



}

async function initMegaLog(context, event) {

    megaLogObject = new Object();
    megaLogObject["status"] = "ok";
    errorIsPresent = false;
    debug_mode = true;
    logCounter = 0;
    final_error_type = "NO_ERROR";
    time_start = new Date().getTime();




   // var my_user_id = "UNKNOWN";
//if(context != null && context.identity != null ){
//    my_user_id = context.identity.cognitoIdentityId;
//}



    var my_user_id = context.identity.cognitoIdentityId;
    var operation = event.operation;
    var gem_id = event.gem_id;

      console.warn(JSON.stringify(context["clientContext"], null, 4));


    var appVersion = context["clientContext"]["client"]["app_version_name"];
    var appTitle = context["clientContext"]["client"]["app_title"];
    var phonePlatform = context["clientContext"]["env"]["platform"];
    var phoneOSVersion = context["clientContext"]["env"]["platform_version"];
    var phoneModel = context["clientContext"]["env"]["model_version"];
    var appPackageName = context["clientContext"]["client"]["app_package_name"];

    var functionVersion = process.env["AWS_LAMBDA_FUNCTION_VERSION"];

    var appShortName = "";

    var appName = "galatea"; // default

    if (appPackageName == "com.galateausa.momentoapp" || appPackageName == "com.galatea.momentopearl") { //for iOS or Android we have different bundle ID's
        appShortName = "g";
        appName = "galatea";
    }
    else if (appPackageName == "com.nfcunited.app" || appPackageName == "com.nfcunited") { //for iOS or Android we have different bundle ID's
        appShortName = "n";
        appName = "nfcunited";
    }
    else if (appPackageName == "com.momentonfc") { //for 'Momento NFC App' we have the same bundle ID for  iOS and Android!!!
        appShortName = "m";
        appName = "momento";
    }  
    else if (appPackageName == "com.momentonfc.desktop") { //for 'Momento Desktop NFC app'
        appShortName = "m";
        appName = "momento";
    }



    var combinedName = appShortName + ":";

    var appPlatformShort = "";
    if (phonePlatform == "iOS") {
        appPlatformShort = "i";
    } else if (phonePlatform == "Windows") {
        appPlatformShort = "w";
    }
    else {
        appPlatformShort = "a";
    }

    combinedName += appPlatformShort;
    combinedName += ":";
    combinedName += appVersion;

    //app type + platform + version name

    megaLog({ channel: "app", data: combinedName, log_line: false });


    if (my_user_id !== undefined) { //logging email address

        const my_email_request = await getUserEmailFromId(my_user_id);
        megaLog({ channel: "username", data: my_email_request.body, log_line: false });
    }

    if (gem_id !== undefined) { //loggin gem_id, if present
        megaLog({ channel: "gem_id", data: gem_id, log_line: false });

    }

    megaLog({ channel: "operation_type", data: operation, log_line: false }); //logging operation type

    if (my_user_id !== undefined) { //logging user id
        megaLog({ channel: "userid", data: my_user_id, log_line: false });
    }

    megaLog({ channel: "user_detailed_info", data: context["clientContext"], log_line: false });


    megaLog({ channel: "ver", data: appVersion, log_line: false });
    megaLog({ channel: "app_type", data: appShortName, log_line: false });
    megaLog({ channel: "app_platform", data: appPlatformShort, log_line: false });
    megaLog({ channel: "function_version", data: functionVersion, log_line: false });
    megaLog({ channel: "app_name", data: appName, log_line: false });



    megaLogObject["extra_logs"] = new Array(); //??? in order for the logs to be in the list before debug data, etc.
    megaLog({ channel: "debug:context", data: context, log_line: false });
    megaLog({ channel: "debug:event", data: event, log_line: false });


}
