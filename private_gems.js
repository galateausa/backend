var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

require('./mega_logger.js')();
/*global  MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/

/*global DB_PRIAVATE_GEMS*/

module.exports = function() {

    this.GetPrivateGem = async({gem_id, user_id} = {}) => {
        return await getPrivateGem({gem_id, user_id});    
    };

    this.GetPrivateGemsForUser = async({ user_id } = {}) => {
        return await getPrivateGemsForUser({ user_id });
    };

    this.WriteItemsToPrivateGem = async({ gem_id, user_id, new_items, new_name } = {}) => {
        return await writeItemsToPrivateGem({ gem_id, user_id, new_items, new_name });
    };

    this.MigratePublicDataToNewOwner = async({ gem_id, from_user, to_user } = {}) => {
        return await migratePublicDataToNewOwner({ gem_id, from_user, to_user });
    };
    
    this.DeletePrivateGem = async({ gem_id, user_id } = {}) => {
     //   if (typeof ignore_error === undefined) {
     //       ignore_error = false;
     //   }
        return await deletePrivateGem({ gem_id, user_id });
    };

};

async function deletePrivateGem({gem_id, user_id}) {

    var params = {
        TableName: DB_PRIAVATE_GEMS,
        Key: { "gem_id": gem_id, "user_id": user_id }
    };


    MegaLog({ data: 'Deleting private gem data for user ' + user_id + ' and gem_id:' + gem_id });
    try {
        const data = await docClient.delete(params).promise();
        return;
    }
    catch (error) {
             MegaLogError({type:"ERROR_DB_PROBLEM4", error:error});
        
        return;
    }

}

async function getPrivateGem({gem_id, user_id}) {

    var params = {
        TableName: DB_PRIAVATE_GEMS,
        Key: { "gem_id": gem_id, "user_id": user_id }
    };


    MegaLog({ data: 'Getting private gem data for user ' + user_id + ' and gem_id:' + gem_id });
    try {
        const data = await docClient.get(params).promise();
        if (data.Item) {
             MegaLog({channel:"big data", data:[data.Item]});
            return data.Item;
        }
        else {
            MegaLogError({comments:"data.Item is not present"});
            return;
        }
    }
    catch (error) {
        MegaLogError({type:"ERROR_DB_PROBLEM3", error:error});
        return;
    }

}


async function writeItemsToPrivateGem({ gem_id, user_id, new_items, new_name } = {}) {
    var params = {
        Key: {
            "gem_id": gem_id,
            "user_id": user_id
        },
        TableName: DB_PRIAVATE_GEMS,
        UpdateExpression: 'set #items=:items_new , #gem_name=:gem_name',
        ExpressionAttributeNames: {
            '#items': 'items',
            '#gem_name': 'gem_name'
        },
        ExpressionAttributeValues: {
            ':items_new': new_items,
            ':gem_name': new_name

        },
        ReturnValues: 'NONE' //UPDATED_NEW //ALL_NEW
    };
    if (typeof new_name === "undefined") {
        params = {
            Key: {
                "gem_id": gem_id,
                "user_id": user_id
            },
            TableName: DB_PRIAVATE_GEMS,
            UpdateExpression: 'set #items=:items_new',
            ExpressionAttributeNames: {
                '#items': 'items'
            },
            ExpressionAttributeValues: {
                ':items_new': new_items
            },
            ReturnValues: 'NONE' //UPDATED_NEW
        };
    }
    
    MegaLog({  data: "gem_id:" + gem_id + " user_id:" + user_id  + " new_name:"+ new_name });

    MegaLog({channel:"big data", data: params });

    try {
        const data = await docClient.update(params).promise();
            MegaLog({channel:"big data", data: data });

        return;
    }
    catch (error) {

        MegaLogError({ error, params, comments: "DB error" });
        return;
    }
}

async function getPrivateGemsForUser({ user_id } = {}) {
    var params = {
        TableName: DB_PRIAVATE_GEMS,
        IndexName: "user_id-index",
        KeyConditionExpression: "#user_id = :user_id",
        ExpressionAttributeNames: {
            "#user_id": "user_id"
        },
        ExpressionAttributeValues: {
            ":user_id": user_id
        }
    };

    MegaLog({channel:"big data",data:params});

    try {
        const data = await docClient.query(params).promise();
    MegaLog({channel:"big data",data:data});

        return data.Items;
    }
    catch (error) {
                MegaLogError({ error, params, comments: "ERROR_DB_PROBLEM" });

        return;
    }

}

async function writeIToPrivateGem(gem_id, user_id, new_items) {

    var params = {
        Key: {
            "gem_id": gem_id,
            "user_id": user_id
        },
        TableName: DB_PRIAVATE_GEMS,
        UpdateExpression: 'set #items=:items_new',
        ExpressionAttributeNames: {
            '#items': 'items'
        },
        ExpressionAttributeValues: {
            ':items_new': new_items
        },
        ReturnValues: 'ALL_NEW' //UPDATED_NEW
    };
    //MegaLog(params);
    try {
        const data = await docClient.update(params).promise();
        //MegaLog(data);
        return;
    }
    catch (error) {
                        MegaLogError({ error, params, comments: "ERROR_DB_PROBLEM" });
        return;
    }
}

async function migratePublicDataToNewOwner({ gem_id, from_user, to_user } = {}) {

    var data_previous_owner_item = await getPrivateGem({gem_id:gem_id, user_id:from_user});

    var publicObjectsWithNewId = [];

    MegaLog({ data: "Starting migration." });
       

    if (NoErrors() == true) {
        MegaLog({ data: 'Found old previous record  items:' + JSON.stringify(data_previous_owner_item, null, 2) });

        var gem_items = data_previous_owner_item.items;
        
         MegaLog({ data: data_previous_owner_item });
        MegaLog({ data: data_previous_owner_item.items });
        MegaLog({ data: gem_items });
        
        if (gem_items && gem_items.length) {
            var filtered_items = gem_items.filter(function(task) {
                return (task.public == true || task.type == "bible");
            });
            var filtered_items_count = Object.keys(filtered_items).length;
            MegaLog('filtered_items_count', filtered_items_count);

            if (!filtered_items_count || filtered_items_count == 0) {
                MegaLog({ data: 'no public records and need to select one private:' + gem_items + " " + gem_items[0] });
                var previousItem = gem_items[0];
                filtered_items.push(previousItem);
                filtered_items_count = 1;
            }
            MegaLog({ data: 'Found old public items:' + JSON.stringify(filtered_items, null, 2) });


            for (var i = 0; i < filtered_items.length; i++) {
                var tempObject = filtered_items[i];
                tempObject['original_id'] = tempObject['id'];
                tempObject['id'] = crypto.randomBytes(20).toString('hex');
                if (tempObject['type'] == "bible") {
                    tempObject['public'] = true;
                }
                else {
                    tempObject['public'] = true; // ALWAYS SHOULD COPY PUBLIC ITEMS
                }
                publicObjectsWithNewId[i] = tempObject;
            }
            MegaLog({ data: 'Replaced ids of old ojects and added a new one:' + JSON.stringify(publicObjectsWithNewId, null, 2) });
            var gem_name = data_previous_owner_item.gem_name ? data_previous_owner_item.gem_name : "empty name";


        }
        else {
            MegaLog({ data: "No records from the previous owner1. This is weird and should not happen3" });
        }

    }
    else {
        MegaLog({ data: "No records from the previous owner2. This is weird and should not happen3" });
    }


    if (publicObjectsWithNewId.length > 0) {
        var params = {
            Key: {
                "gem_id": gem_id,
                "user_id": to_user
            },
            TableName: DB_PRIAVATE_GEMS,
            UpdateExpression: 'set #markedLocations = list_append(  if_not_exists(#markedLocations, :empty_list) , :newRecord)', // gem_name = :newGemName ,
            ExpressionAttributeNames: {
                '#markedLocations': 'items'

            },
            ExpressionAttributeValues: {
                ':newRecord': publicObjectsWithNewId,
                ':empty_list': [],
            },
            ReturnValues: 'NONE'
        };

        try {
            const data = await docClient.update(params).promise();
            MegaLog({ data: "UpdateItem migratePublicDataToNewOwner succeeded:" });
            return;
        }
        catch (error) {
            MegaLogError({ type: "ERROR_DB_PROBLEM", error: error, comments: "Unable to update item" ,params:params});
            return;
        }
    }
    else {
        MegaLog({ data: "No records to migrate. Not making a request." });

    }



}
