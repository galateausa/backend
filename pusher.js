var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
var querystring = require('querystring');
var http = require('http');

//var request = require('request');
async function GetUserDeviceIDs() {

    // var date_now = new Date();
    // var date_connected = date_now.getTime();
    // var userTableParams = {
    //     Key: { "email_id": my_email, "sub_id": my_sub_id },
    //     TableName: "users",
    //     UpdateExpression: 'set user_id = :user_id, date_connected = :date_connected,date_connected_human = :date_connected_human',
    //     ExpressionAttributeValues: {
    //         ':user_id': my_user_id,
    //         ':date_connected': date_connected,
    //         ':date_connected_human': date_now.toISOString(),
    //     },
    //     ReturnValues: 'ALL_NEW' //UPDATED_NEW
    // };

    // try {
    //     const data = await docClient.update(userTableParams).promise();
    //     console.log(data);
    //     return true;
    // }
    // catch (error) {
    // console.log(error);
    //     return false;
    // }
}
async function sendPushNotification(other_user_id, title, message, push_extra_data) {

    var interest = '77737' + other_user_id + '858';
    var interest_hashed1 = crypto.createHash('md5').update(interest).digest("hex");
    var interest_hashed2 = crypto.createHash('md5').update(interest_hashed1).digest("hex");
    var interest_hashed3 = crypto.createHash('md5').update(interest_hashed2).digest("hex");



    if (push_extra_data === undefined) {
        push_extra_data = { "push_type": "test" };
    }

    var apsObject = {
        "sound": "default",
        "alert": {
            "title": title,
            "body": message
        }
    };
    var combinedApsObject = Object.assign(push_extra_data, apsObject);
    var requestJSON = {
        "interests": [interest_hashed3],
        "apns": {
            "aps": combinedApsObject
        }
    };

    //var post_data_old = '{"interests":["' + interest_id + '"],"apns":{"aps":{"push_type" : "'+push_type+'","sound" : "default","alert":{"title":"' + title + '","body":"' + message + '"}}}}';
    var post_data = JSON.stringify(requestJSON);
    const post_options = {
        host: 'c9e51363-022e-4089-b31f-acd26b59e5a6.pushnotifications.pusher.com',
        port: '80',
        path: '/publish_api/v1/instances/c9e51363-022e-4089-b31f-acd26b59e5a6/publishes',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer 103E2C34D5AF6A5FC37BE7D078F9499E236AFC0E9324373256CB8947775820AF',
            'Content-Length': Buffer.byteLength(post_data)
        }
    };

    MegaLog([post_data, post_options]);
    // Set up the request

    // Return new promise
    return new Promise(function(resolve, reject) {

        var post_req = http.request(post_options, function(res) {
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                MegaLog(['Response: ' + chunk]);
                //context.succeed();
                // console.log(chunk);
                MegaLog([chunk]);

                resolve(chunk);

            });
            res.on('error', function(e) {
                //console.log("Got error: " + e.message);
                context.done(null, 'FAILURE');
                //console.log(e);
                //console.log(e.message);
                MegaLog(['FAILURE: ' + e]);

                reject(e.message);
            });

        });

        // post the data
        post_req.write(post_data);
        post_req.end();


    });


}

module.exports = function() {
    //  this.sum = function(a, b) { return a + b };

    this.SendPushNotofication = async(other_user_id, title, message, push_extra_data) => {

        await sendPushNotification(other_user_id, title, message, push_extra_data);
        //console.log('push result');
        return "success";
    };
};
