var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

require('./mega_logger.js')();
/*global  MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/
/*global GetNewTagName*/

require('./private_gems.js')();
/*global GetPrivateGem*/
/*global WriteItemsToPrivateGem*/

/*global DB_PRIAVATE_GEMS*/

module.exports = function() {

    this.CopyMyRecord = async(user_action_id, first_gem_id, second_gem_id, record_source_index_id, record_destination_index_id) => {
        return await copyMyRecord(user_action_id, first_gem_id, second_gem_id, record_source_index_id, record_destination_index_id);
    };

    this.MoveMyRecord = async(user_action_id, gem_id, record_source_index_id, record_destination_index_id) => {
        return await moveMyRecord(user_action_id, gem_id, record_source_index_id, record_destination_index_id);
    };


    this.RenameMyRecord = async(user_action_id, gem_id, record_id, new_record_name) => {
        return await renameMyRecord(user_action_id, gem_id, record_id, new_record_name);
    };

    this.DeleteMyRecord = async(user_action_id, gem_id, record_id) => {
        return await deleteMyRecord(user_action_id, gem_id, record_id);
    };

    this.MakeMyRecordPublic = async(user_action_id, gem_id, record_id) => {
        return await makeMyRecordPublic(user_action_id, gem_id, record_id);
    };

    this.AddRecordToPrivateGem = async({ type, text, title, gem_id, gem_owner_id, file, migrate_from } = {}) => {
        return await addRecordToPrivateGem({ type, text, title, gem_id, gem_owner_id, file, migrate_from });
    };

    this.GiftMyRecord = async(user_action_id, first_gem_id, second_gem_id, record_id) => {
        return await giftMyRecord(user_action_id, first_gem_id, second_gem_id, record_id);
    };

};


async function copyMyRecord(user_action_id, first_gem_id, second_gem_id, record_source_index_id, record_destination_index_id) {

    var private_gem_data = await GetPrivateGem({ gem_id: first_gem_id, user_id: user_action_id });
    var second_private_gem_data = await GetPrivateGem({ gem_id: second_gem_id, user_id: user_action_id });

    MegaLog({ data: private_gem_data });
    MegaLog({ data: second_private_gem_data });

    if (NoErrors() == true) { // both gems are present and not empty


        var first_gem_items = private_gem_data.items;
        var item_to_copy = first_gem_items[record_source_index_id];

        var record_id = crypto.randomBytes(20).toString('hex'); //generating new record id
        item_to_copy["id"] = record_id;

        var second_gem_items = second_private_gem_data.items;
        second_gem_items.splice(record_destination_index_id, 0, item_to_copy);

        var move_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: second_gem_id, user_id: user_action_id, new_items: second_gem_items });
        return move_my_record_final_result;

    }
    else {
        //megaLogger["extra_logs"].push('There was an error when asking for the first gems data', JSON.stringify(err_firstGemGetRequest, null, 2));
        //OR
        //megaLogger["extra_logs"].push('The first gem appears to be empty');
        //OR
        //megaLogger["extra_logs"].push('There was an error when asking for the second gems data', JSON.stringify(err_secondGemGetRequest, null, 2));
        //OR
        //megaLogger["extra_logs"].push('The second gem appears to be empty');


    }

}

async function moveMyRecord(user_action_id, gem_id, record_source_index_id, record_destination_index_id) {

    var private_gem_data = await GetPrivateGem({ gem_id: gem_id, user_id: user_action_id });
    MegaLog({ data: private_gem_data });
    if (NoErrors() == true) {

        var new_items = private_gem_data.items;
        new_items.splice((record_destination_index_id < 0 ? new_items.length + record_destination_index_id : record_destination_index_id), 0, new_items.splice(record_source_index_id, 1)[0]);

        var rename_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: gem_id, user_id: user_action_id, new_items: new_items });
        return rename_my_record_final_result;
    }

}

async function renameMyRecord(user_action_id, gem_id, record_id, new_record_name) {

    var private_gem_data = await GetPrivateGem({ gem_id: gem_id, user_id: user_action_id });
    MegaLog({ data: private_gem_data });
    if (NoErrors() == true) {
        var new_items = private_gem_data.items;

        for (var i = 0; i < new_items.length; i++) {
            if (new_items[i]['id'] == record_id) {
                new_items[i]['title'] = new_record_name;
            }
        }
        var rename_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: gem_id, user_id: user_action_id, new_items: new_items });
    }

}

async function makeMyRecordPublic(user_action_id, gem_id, record_id, new_record_name) {

    var private_gem_data = await GetPrivateGem({ gem_id: gem_id, user_id: user_action_id });
    MegaLog({ data: private_gem_data });
    if (NoErrors() == true) {
        var new_items = private_gem_data.items;

        for (var i = 0; i < new_items.length; i++) {
            if (new_items[i]['id'] == record_id) {
                new_items[i]['public'] = !new_items[i]['public'];
            }
        }
        var rename_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: gem_id, user_id: user_action_id, new_items: new_items });
    }

}

async function deleteMyRecord(user_action_id, gem_id, record_id, new_record_name) {

    //  if (record_id == "bible") {
    //                 service.SendBackError(megaLogger, constants.ERROR_REMOVING_BIBLE_PEARL);
    // }

    var private_gem_data = await GetPrivateGem({ gem_id: gem_id, user_id: user_action_id });

    if (NoErrors() == true) {
        var new_items = private_gem_data.items;


        var new_items2 = [];
        for (var i = 0; i < new_items.length; i++) {
            var tempObject = new_items[i];
            if (tempObject['id'] != record_id) {
                new_items2.push(tempObject);
            }
            else {
                //saving deleted record just in case!
                MegaLog({ channel: "deleted_record", data: tempObject, log_line: false });

            }
        }

        var rename_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: gem_id, user_id: user_action_id, new_items: new_items2 });
    }

}

async function addRecordToPrivateGem({ type, text, title, gem_id, gem_owner_id, file, migrate_from } = {}) {
    //gem_default_name
    MegaLog({ data: "file:" + file });

    var gem_default_name = GetNewTagName(); // "My new NFC tag" or  "My new gem"
    //  var newGemDefaultName = app_system_owner_user_id === "GALATEA" ? "My gem name" : "My tag name";
    // var gem_default_name2 = gem_default_name ? gem_default_name : newGemDefaultName;
    var record_id = crypto.randomBytes(20).toString('hex');
    var date = new Date().getTime();
    var new_record = {
        "type": type,
        "public": true,
        "text": text,
        "id": record_id,
        "date": date
    };

    if (typeof titleText !== 'undefined') { //for text records
        new_record["title"] = title;
        MegaLog({ data: "writing title" });
    }
    else {
        MegaLog({ data: "not writing title" });
    }

    if (type == "sound" || type == "image" || type == "video" || type == "pdf") {
        new_record["file"] = file;
    }

    var params = {
        Key: {
            "gem_id": gem_id,
            "user_id": gem_owner_id
        },
        TableName: DB_PRIAVATE_GEMS,
        UpdateExpression: 'set last_date = :last_date , gem_name = if_not_exists(#gamName,:newName)  , #markedLocations = list_append( :newRecord , if_not_exists(#markedLocations, :empty_list) )',
        ExpressionAttributeNames: {
            '#markedLocations': 'items',
            '#gamName': 'gem_name'
        },
        ExpressionAttributeValues: {
            ':newRecord': [new_record],
            ':empty_list': [],
            ':newName': gem_default_name,
            ':last_date': date
        },
        ReturnValues: 'NONE' //UPDATED_NEW
    };

    //MegaLog({ data: ['Params for writing record :', params] });
    try {
        const data = await docClient.update(params).promise();
        MegaLog({ data: "writing record to private_gems succeeded:" });


    }
    catch (error) {
        MegaLogError({ type: "ERROR_DB_PROBLEM", error: error, comments: "Unable to update item", params: params });
        return;
    }


    if (migrate_from !== undefined) {
        MegaLog({ data: "we are changing ownership to a new person and migrating data from:" + migrate_from });
        await ChangeOwnerForGem({ new_owner: gem_owner_id, gem_id: gem_id });
        await MigratePublicDataToNewOwner({ gem_id: gem_id, from_user: migrate_from, to_user: gem_owner_id });
        // = {}
        return;
    }
    else {
        MegaLog({ data: "we are NOT changing ownership to a new person and NOT migrating data" });
        return;
    }

}


async function giftMyRecord(user_action_id, first_gem_id, second_gem_id, record_id) {

    var first_private_gem_data = await GetPrivateGem({ gem_id: first_gem_id, user_id: user_action_id });
    if (NoErrors() == true) { // both gem is present in the DB
        var firstGemItems = first_private_gem_data.items;
        var filteredItems = firstGemItems.filter(x => x.id === record_id);

        if (filteredItems.length == 1) {

            var itemToCopy = filteredItems[0];
            //megaLogger["extra_logs"].push('Found item:', JSON.stringify(itemToCopy, null, 2));

            itemToCopy["public"] = true;

            var gem_data = await GetGem(second_gem_id);
            if (NoErrors() == true) { // gem data is present
                var active_user_id_found = gem_data.active_user_id;
                if (!active_user_id_found) { //this means that noone wrote to this gem yet and it is empty
                    // megaLogger["extra_logs"].push('pullActiveUserIDREquest ok.-3');
                    // service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err_pull_pullActiveUserIDREquest);
                    MegaLogError({ type: "ERROR_DB_PROBLEM", error: "", comments: "Active user is not found", params: "" });

                }
                else {
                    var secondGemData = await GetPrivateGem({ gem_id: second_gem_id, user_id: active_user_id_found });
                    if (NoErrors() == true) { //second gem private data found for gem active user

                        var record_id = crypto.randomBytes(20).toString('hex');
                        itemToCopy["id"] = record_id;

                        var secondGemItems = secondGemData.items;
                        secondGemItems.splice(0, 0, itemToCopy);

                        var move_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: second_gem_id, user_id: active_user_id_found, new_items: secondGemItems });

                    }
                }
            }
        }
        else {
            MegaLogError({ type: "ERROR_DB_PROBLEM", error: "", comments: "Unable to find the record with id provided", params: "" });
            //               // megaLogger["extra_logs"].push('filteredItemsLength', filteredItemsLength);
            //     megaLogger["extra_logs"].push('id', first_record_id);
            //     megaLogger["extra_logs"].push('first_gem_id', first_gem_id);
            //     megaLogger["extra_logs"].push('second_gem_id', second_gem_id);
            //     megaLogger["extra_logs"].push('first_record_id', first_record_id);
            //     megaLogger["extra_logs"].push('Found items', JSON.stringify(firstGemItems, null, 2));
            //     service.SendBackError(megaLogger, constants.ERROR_DB_PROBLEM, err_firstGemGetRequest);
        }
    }
}
