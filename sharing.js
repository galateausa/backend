var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

require('./mega_logger.js')();
/*global  MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/


require('./private_gems.js')();
/*global GetPrivateGem*/
/*global WriteItemsToPrivateGem*/


module.exports = function() {

    this.AddSharingRequest = async(my_email, my_id, user_email, user_id, gem_id, gem_name) => {

        // handle if the request is already present
        // this is ignored for now
        var request_id = crypto.randomBytes(20).toString('hex');
        var options = {
            Item: {
                "user_id": user_id,
                "gem_id": gem_id,
                "user_email": user_email,
                "owner_user_id": my_id,
                "owner_user_email": my_email,
                "gem_name": gem_name,
                "request_id": request_id,
                "status": false,
                "notifications": true
            },
            TableName: "sharing_requests"
        };

        try {
            const data = await docClient.put(options).promise();
            //console.log(data);
            return { error: false, body: data };
        }
        catch (error) {
            return { error: true, body: "DB error" };
        }
    };


    this.GetListOfActiveSharingRequests = async(my_id) => {
        return await getListOfActiveSharingRequests(my_id);
    };

    this.GetListOfActiveSharingRequestsForOwner = async(my_id) => {
        return await getListOfActiveSheringRequestsForOwner(my_id);
    };


    this.GetSlaveUserForGem = async(my_id, my_gem) => {

        var sharing_gems_params = {
            TableName: "sharing_requests",
            IndexName: "owner_user_id-index",
            KeyConditionExpression: "owner_user_id = :owner_user_id",
            ExpressionAttributeValues: {
                ":owner_user_id": my_id,
                ":gem_id": my_gem,
                ":status": true
            },
            ExpressionAttributeNames: {
                '#status': 'status',
                '#gem_id': 'gem_id'
            },
            FilterExpression: "#status = :status AND #gem_id = :gem_id"
        };

        try {
            const data = await docClient.query(sharing_gems_params).promise();
            if (data.Items && data.Items.length > 0) {
                var firstItem = data.Items[0];
                var slave_user_id = firstItem.user_id;
                MegaLog(data.Items);
                MegaLog(slave_user_id);
                return slave_user_id;
            }
            else {
                // MegaLogError("no items");
                return;
            }
        }
        catch (error) {
            // MegaLogError("DB error", error);
            return;
        }

    };



    this.GetListOfSharingRequests = async(my_id) => {

        var sharing_gems_params = {
            TableName: "sharing_requests",
            IndexName: "user_id-index",
            KeyConditionExpression: "user_id = :user_id",
            //  ExpressionAttributeNames: { "#user_id": "user_id" },
            ExpressionAttributeValues: {
                ":user_id": my_id,
                ":status": false
            },
            ExpressionAttributeNames: {
                '#status': 'status'
            },
            FilterExpression: "#status = :status"
        };

        try {
            const data = await docClient.query(sharing_gems_params).promise();
            if (data.Items && data.Items.length > 0) {
                return data.Items;
            }
            else {

                MegaLog({
                    data: "no items"
                }); // this is normal, no need for panicin with errors
                return;
            }
        }
        catch (error) {
            MegaLogError({
                error,
                comments: "DB error"
            });
            return;
        }

    };


    this.GetSharingRequestByID = async(sharing_request_id) => {
        return await getSharingRequestByID(sharing_request_id);
    };


    this.TurnSharingNotification = async(user_id, gem_request_id, sharing_notification_sound_on_or_off) => {
        //console.log(sharing_notification_sound_on_or_off);
        //console.log(typeof sharing_notification_sound_on_or_off);

        var params = {
            Key: {
                "user_id": user_id,
                "gem_id": gem_request_id
            },
            TableName: "sharing_requests",
            UpdateExpression: 'set #notifications = :notifications ',
            ExpressionAttributeNames: {
                '#notifications': 'notifications'
            },
            ExpressionAttributeValues: {
                ':notifications': sharing_notification_sound_on_or_off,
            },
            ReturnValues: 'ALL_NEW' //UPDATED_NEW
        };

        //console.log(params);
        try {
            const data = await docClient.update(params).promise();
            //console.log("111" + data);
            // if (data.Items && data.Items.length == 1) {
            //     return { error: false, body: data.Items[0] };
            // }
            // else {
            return { error: false, body: "request accepted" };
            // }
        }
        catch (error) {
            //console.log(error);

            return { error: true, body: "DB error" };
        }

    };

    this.AcceptSharingRequest = async(user_id, gem_request_id) => {

        var params = {
            Key: {
                "user_id": user_id,
                "gem_id": gem_request_id
            },
            TableName: "sharing_requests",
            UpdateExpression: 'set #status = :status ',
            ExpressionAttributeNames: {
                '#status': 'status'
            },
            ExpressionAttributeValues: {
                ':status': true,
            },
            ReturnValues: 'ALL_NEW' //UPDATED_NEW
        };

        //console.log(params);
        try {
            const data = await docClient.update(params).promise();
            //console.log("111" + data);
            // if (data.Items && data.Items.length == 1) {
            //     return { error: false, body: data.Items[0] };
            // }
            // else {
            return { error: false, body: "request accepted" };
            // }
        }
        catch (error) {
            //console.log(error);

            return { error: true, body: "DB error" };
        }

    };


    this.RemoveSharingRequest = async(user_id, gem_request_id) => {
        return await removeSharingRequest(user_id, gem_request_id);
    };

    this.GetUserIDForActionOnGem = async(my_user_id, gem_id) => {
        return await getUserIDForActionOnGem(my_user_id, gem_id);
    };

    this.DoIShareThisGem = async(my_user_id, gem_id) => {
        return await doIShareThisGem(my_user_id, gem_id);
    };

    this.IsSharingActiveForMeAndThisGem = async(my_user_id, gem_id) => {
        return await isSharingActiveForMeAndThisGem(my_user_id, gem_id);
    };

    this.StopSharingGem = async({ sharing_request_id, gem_id, create_copy_for_slave } = {}) => {

        return await stopSharingGem({ sharing_request_id, gem_id, create_copy_for_slave });
    };

    this.GetListOfActiveSheringRequestsForGem = async(gem_id) => {
        return await getListOfActiveSheringRequestsForGem(gem_id);
    };

    this.SharingRoleForGem = async({ my_user_id, gem_id } = {}) => {
        return await sharingRoleForGem({ my_user_id, gem_id });
    };


};

async function removeSharingRequest(user_id, gem_request_id) {

    var params = {
        TableName: "sharing_requests",
        Key: {
            "user_id": user_id,
            "gem_id": gem_request_id
        }
    }
    //console.log(params);
    try {
        const data = await docClient.delete(params).promise();
        //console.log("111 " + data);
        // if (data.Items && data.Items.length == 1) {
        //     return { error: false, body: data.Items[0] };
        // }
        // else {
        MegaLog({ data: "Sharing request removed from the database." });
        return;
        // }
    }
    catch (error) {
        MegaLogError({
            error,
            comments: "DB error"
        });
        return;
    }

};

async function getSharingRequestByID(sharing_request_id) {
    MegaLog({
        data: "sharing_request_id:" + sharing_request_id
    });
    var sharing_gems_params = {
        TableName: "sharing_requests",
        IndexName: "request_id_index",
        KeyConditionExpression: "request_id = :request_id",
        //  ExpressionAttributeNames: { "#request_id": "user_id" },
        ExpressionAttributeValues: { ":request_id": sharing_request_id }
    };

    try {
        const data = await docClient.query(sharing_gems_params).promise();
        if (data.Items && data.Items.length == 1) {
            return data.Items[0];
        }
        else {
            MegaLogError({
                comments: "no items"
            });
            return;
        }
    }
    catch (error) {
        //console.log(error);
        MegaLogError({
            error,
            comments: "DB error"
        });
        return;

    }

}

async function stopSharingGem({ sharing_request_id, gem_id, create_copy_for_slave = true } = {}) {
                MegaLog({ data: "stopSharingGem:create_copy_for_slave="+create_copy_for_slave });

    if (sharing_request_id === undefined) {
        if (gem_id === undefined) {
            //nothing specifyed. cannot stop sharing.
            MegaLogError({ error: "not enough data" });
            return;
        }
        else {
            var sharing_requests_data = await getListOfActiveSheringRequestsForGem(gem_id);
            if (sharing_requests_data.length > 0) { //sharing is active. we are finishing it (copying to all the users and deleting the request)
                sharing_request_id = sharing_requests_data[0].request_id;
            }
        }
    }
    var sharingRequestInfo = await getSharingRequestByID(sharing_request_id);

    const user_request_id = sharingRequestInfo.user_id;
    const gem_request_id = sharingRequestInfo.gem_id;

    console.log("create_copy_for_slave:" + create_copy_for_slave);
    if (create_copy_for_slave) {
        var _owner_user_id = sharingRequestInfo["owner_user_id"];

        const private_gem_data = await GetPrivateGem({ gem_id: gem_request_id, user_id: _owner_user_id });
        var old_items = private_gem_data.items;

        //change ids here later
        // for (var i = 0; i < new_items.length; i++) {
        //             if (new_items[i]['id'] == record_id) {
        //                 new_items[i]['title'] = new_record_name;
        //             }
        //         }
        var gem_current_name = private_gem_data.gem_name;

        var rename_my_record_final_result = await WriteItemsToPrivateGem({ gem_id: gem_request_id, user_id: user_request_id, new_items: old_items, new_name: gem_current_name });
    }

    const stopSharingRequestTask = await RemoveSharingRequest(user_request_id, gem_request_id);



}


async function getUserIDForActionOnGem(my_user_id, gem_id) {

    MegaLog({ data: "gem_id:" + gem_id + " my_user_id:" + my_user_id + " " });
    var gem_data = await GetGem(gem_id);
    if (NoErrors() == true) { //gem was found in the db


        var am_i_the_gem_owner = await AmITheGemOwner(my_user_id, gem_id, gem_data);

        if (am_i_the_gem_owner == true) {
            return my_user_id; //we own this gem so we will use our id on the private_gems table
        }
        else {
            //okey, we do not own this gem so maybe someone is sharing this gem with us and we are not hackers
            var do_we_share_this_gem = await doIShareThisGem(my_user_id, gem_id);

            if (do_we_share_this_gem == true) { //someone is sharing this gem with us, so we can use owner_id and do some actions with it directly in the private_gems table

                var private_gem_id_to_use = await GetGemOwnerID(gem_id, gem_data);
                MegaLog({ data: "private_gem_id_to_use:" + private_gem_id_to_use });
                return private_gem_id_to_use;
            }
            else {
                // we dont own the gem and we are not sharing it with anywone BUT we can make all the things with the local copy of that gem. so no error here.
                MegaLog({ data: "we are here2" });
                return my_user_id; //carefull, we don't want to accidently change the ownership of another person
            }

        }
    }
    else {
        MegaLog({ data: "gem was not found in DB" });
    }
}

async function sharingRoleForGem({ my_user_id, gem_id } = {}) {

    var sharing_requests_data = await getListOfActiveSheringRequestsForGem(gem_id);

    var gem_sharing_info = { gem_shared: false, share_master: false, share_slave: false, i_am_involved_in_sharing: false };
    if (sharing_requests_data !== undefined) {
        for (var i = 0; i < sharing_requests_data.length; i++) {
            //got at least one item = that means that the gem is shared;
            gem_sharing_info.gem_shared = true;
            var temp_sharing_info = sharing_requests_data[i];
            var owner_user_id = temp_sharing_info.owner_user_id;
            var slave_user_id = temp_sharing_info.user_id;
            if (owner_user_id === my_user_id) {
                gem_sharing_info.share_master = true;
            }
            else if (slave_user_id === my_user_id) {
                gem_sharing_info.share_slave = true;
            }

        }
        if (gem_sharing_info.share_master == true || gem_sharing_info.share_slave == true) {
            gem_sharing_info.i_am_involved_in_sharing = true;
        }
    }


    MegaLog({ data: "gem_shared:"+gem_sharing_info.gem_shared+ " share_master:"+gem_sharing_info.share_master+ " share_slave:"+gem_sharing_info.share_slave+" i_am_involved_in_sharing:"+gem_sharing_info.i_am_involved_in_sharing});
    
    return gem_sharing_info;
}


async function doIShareThisGem(my_user_id, gem_id) {

    MegaLog({ data: 'Starting request. my_user_id:' + my_user_id + '  gem_id:' + gem_id });

    if (my_user_id === undefined) {
        MegaLogError({ error: "my_user_id is undefined" });
        return;
    }
    if (gem_id === undefined) {
        MegaLogError({ error: "gem_id is undefined" });
        return;
    }

    var params = {
        TableName: "sharing_requests",
        Key: {
            "gem_id": gem_id,
            "user_id": my_user_id
        },

    };
    try {
        const data = await docClient.get(params).promise();
        if (data.Item) {
            MegaLog({ data: "we are sharing this gem with someone." });
            //TODO check if the sharing is active!!!!
            return true;
        }
        else {
            MegaLog({ data: "data.Item is not present. we are not sharing this gem with anyone" });
            return false;
        }
    }
    catch (error) {
        MegaLog({ comments: "we are not sharing this gem with anyone. possible hacker alert.but thats not an error", data: params, error: error });
        return false;
    }

}

async function isSharingActiveForMeAndThisGem(my_user_id, gem_id) {
    MegaLog({ data: 'Starting request. my_user_id:' + my_user_id + '  gem_id:' + gem_id });

    var params = {
        TableName: "sharing_requests",
        Key: {
            "gem_id": gem_id,
            "user_id": my_user_id
        },

    };
    try {
        const data = await docClient.get(params).promise();
        if (data.Item) {
            MegaLog({ data: "we are sharing this gem with someone." });
            //TODO check if the sharing is active!!!!
            return true;
        }
        else {
            MegaLog({ data: "data.Item is not present. we are not sharing this gem with anyone" });
            return false;
        }
    }
    catch (error) {
        MegaLog("we are not sharing this gem with anyone.hacker alert", params, error);
        return false;
    }
}

async function getListOfActiveSheringRequestsForOwner(my_id) {

    var sharing_gems_params = {
        TableName: "sharing_requests",
        IndexName: "owner_user_id-index",
        KeyConditionExpression: "owner_user_id = :owner_user_id",
        //  ExpressionAttributeNames: { "#owner_user_id": "owner_user_id" },
        ExpressionAttributeValues: {
            ":owner_user_id": my_id,
            ":status": true
        },
        ExpressionAttributeNames: {
            '#status': 'status'
        },
        FilterExpression: "#status = :status"
    };

    try {
        const data = await docClient.query(sharing_gems_params).promise();
        MegaLog({ channel:"big data", data: data.Items, comments: "Got sharing data.Count:" + data.Items.length });
        return data.Items;
    }
    catch (error) {
        MegaLogError({ error: error, comments: "DB error" });
    }
}

async function getListOfActiveSharingRequests(my_id) {

    var sharing_gems_params = {
        TableName: "sharing_requests",
        IndexName: "user_id-index",
        KeyConditionExpression: "user_id = :user_id",
        //  ExpressionAttributeNames: { "#user_id": "user_id" },
        ExpressionAttributeValues: {
            ":user_id": my_id,
            ":status": true
        },
        ExpressionAttributeNames: {
            '#status': 'status'
        },
        FilterExpression: "#status = :status"
    };

    try {
        const data = await docClient.query(sharing_gems_params).promise();
        MegaLog({ data: "got " + data.Items.length + " shared gems that I own" });
        return data.Items;
    }
    catch (error) {
        MegaLogError({ error: error, comments: "DB error" });
    }

};


async function getListOfActiveSheringRequestsForGem(gem_id) {

    var sharing_gems_params = {
        TableName: "sharing_requests",
        IndexName: "gem_id-index",
        KeyConditionExpression: "gem_id = :gem_id",
        ExpressionAttributeValues: {
            ":gem_id": gem_id,
            ":status": true
        },
        ExpressionAttributeNames: {
            '#status': 'status'
        },
        FilterExpression: "#status = :status"
    };
    try {
        const data = await docClient.query(sharing_gems_params).promise();
        // MegaLog({ data: "got " + data.Items.length + " shared gems that I own" });
        return data.Items;
    }
    catch (error) {
        MegaLogError({ error: error, comments: "DB error" });
    }
}


// KeyConditionExpression: "user_id = :user_id",
// ExpressionAttributeValues: {
//     ":owner_user_id": owner_user_id,
//     ":status": true
// },
// ExpressionAttributeNames: {
//     '#status': 'status',
//     '#owner_user_id': 'owner_user_id'
// },
// FilterExpression: "#status = :status && #owner_user_id = :owner_user_id"
