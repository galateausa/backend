var crypto = require('crypto');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

require('./mega_logger.js')();
/*global  InitMegaLog*/
/*global  RunMegaLog*/
/*global MegaLogError*/
/*global  MegaLog*/
/*global NoErrors*/
/*global GetTotalExcecutionTime*/

require('./private_gems.js')();
/*global GetPrivateGemsForUser*/
/*global GetPrivateGem*/

require('./gems.js')();
/*global GetGemsForUser*/

require('./sharing.js')();
/*global GetListOfActiveSharingRequestsForOwner*/
/*global GetListOfActiveSharingRequests*/

/*global ERROR_YOU_DONT_HAVE_GEMS*/
/*global USER_FOR_SHARING_NOT_FOUND*/

async function getUserPrefs(my_email, my_sub_id) {

    var user_prefs_params = { Key: { "email_id": my_email, "sub_id": my_sub_id }, TableName: "users" };

    try {
        const data = await docClient.getItem(user_prefs_params).promise();
        if (data.Item) {
            //console.log(data.Item);
            return true;
            // return { error: false, body: data.Items };
        }
        else {
            return false;
        }
    }
    catch (error) {
        return false;
    }
}

async function ActivateUserPrefs(my_email, my_sub_id, my_user_id) {

    var date_now = new Date();
    var date_connected = date_now.getTime();
    var userTableParams = {
        Key: { "email_id": my_email, "sub_id": my_sub_id },
        TableName: "users",
        UpdateExpression: 'set user_id = :user_id, date_connected = :date_connected,date_connected_human = :date_connected_human',
        ExpressionAttributeValues: {
            ':user_id': my_user_id,
            ':date_connected': date_connected,
            ':date_connected_human': date_now.toISOString(),
        },
        ReturnValues: 'ALL_NEW' //UPDATED_NEW
    };

    try {
        const data = await docClient.update(userTableParams).promise();
        //console.log(data);
        return true;
    }
    catch (error) {
        MegaLogError({ type: "ERROR_DB_PROBLEM3", error: error });

        return false;
    }
}

async function getMyGems({ my_user_id } = {}) {

//console.log(__function);
//we are turning debug loggin off since GetMyGems is usually the last operation
    MegaLog({ data: "--------------------------------------------------------------------" });
    MegaLog({ data: "--------------------------------------------------------------------" });
    MegaLog({ data: "---Starting getting gems (GetMyGems) for user::" + my_user_id + "---" });
    MegaLog({ data: "--------------------------------------------------------------------" });
    MegaLog({ data: "--------------------------------------------------------------------" });

    var my_private_gems = await GetPrivateGemsForUser({ user_id: my_user_id });
    MegaLog({ data: my_private_gems.length, comments: "found all my gems" });

    var my_gems = await GetGemsForUser({ user_id: my_user_id });
    MegaLog({ data: my_gems.length, comments: "found my gems that I own" });

    var my_sharing_data_owner = await GetListOfActiveSharingRequestsForOwner(my_user_id);
    MegaLog({ data: my_sharing_data_owner.length, comments: "found sharing gems that I do own" });
    //MegaLog({ data: my_sharing_data_owner });

    var my_sharing_data__not_owner = await GetListOfActiveSharingRequests(my_user_id);
    MegaLog({ data: my_sharing_data__not_owner.length, comments: "found sharing gems that I do NOT own" });
    //MegaLog({ data: my_sharing_data__not_owner });

    if (NoErrors() == true) {
        MegaLog({
            data: "no errors = true"
        });

        var data_gems_sorted = {};

        for (var key in my_gems) {
            if (my_gems.hasOwnProperty(key)) {
                var gemmm_id = my_gems[key]['gem_id'];
                if (my_gems[key]['active_user_id'] == my_user_id) {
                    data_gems_sorted[gemmm_id] = my_gems[key]['is_locked'];
                }
            }
        }

        for (var i = 0; i < my_private_gems.length; i++) {
            var obj_ID = my_private_gems[i]['gem_id'];
            var lockedddd = data_gems_sorted[obj_ID];
            my_private_gems[i]['is_locked'] = lockedddd;

            my_private_gems[i]['is_shared'] = false; //default
            my_private_gems[i]['my_ownership'] = false; //default

            for (var j = 0; j < my_sharing_data_owner.length; j++) {
                var active_sharing_requests_for_owner_result_item = my_sharing_data_owner[j];
                var gem_id_for_active_sharing_me = active_sharing_requests_for_owner_result_item["gem_id"];
                var gem_owner_email = active_sharing_requests_for_owner_result_item["owner_user_email"];
                var gem_slave_mail = active_sharing_requests_for_owner_result_item["user_email"];

                var gem_notifications = active_sharing_requests_for_owner_result_item["notifications"];
                var gem_sharing_request_id = active_sharing_requests_for_owner_result_item["request_id"];

                if (gem_id_for_active_sharing_me == obj_ID) {
                    my_private_gems[i]['is_shared'] = true;
                    my_private_gems[i]['my_ownership'] = true;
                    my_private_gems[i]['sharing_owner_email'] = gem_owner_email;
                    my_private_gems[i]['sharing_slave_email'] = gem_slave_mail;
                    my_private_gems[i]['sharing_notifications'] = gem_notifications;
                    my_private_gems[i]['sharing_request_id'] = gem_sharing_request_id;



                    //my_private_gems[i]['sharing_other_users'] = gem_owner_email;
                    break;
                }
            }


        }

        var data_gems_ = {};

        //adding shared items from other users
        for (var i = 0; i < my_sharing_data__not_owner.length; i++) {

            var elmnt = my_sharing_data__not_owner[i];
            var gem_id_from_element = elmnt.gem_id;
            var owner_user_id_element = elmnt.owner_user_id;
            //console.log(JSON.stringify(elmnt, null, 2));
            //console.log(JSON.stringify(my_sharing_data__not_owner, null, 2));
            var gem_owner_mail = elmnt["owner_user_email"];
            var gem_slave_mail = elmnt["user_email"];

            var gem_notifications = elmnt["notifications"];
            var gem_sharing_request_id = elmnt["request_id"];

            MegaLog({ data: owner_user_id_element + " " + gem_id_from_element + " " + gem_owner_mail });


            var private_elmnt = await GetPrivateGem({ gem_id: gem_id_from_element, user_id: owner_user_id_element });


            // console.log(private_elmnt);
            if (private_elmnt !== undefined) {
                private_elmnt['is_shared'] = true;
                private_elmnt['my_ownership'] = false;
                private_elmnt['sharing_owner_email'] = gem_owner_mail;
                private_elmnt['sharing_slave_email'] = gem_slave_mail;
                private_elmnt['sharing_notifications'] = gem_notifications;
                private_elmnt['sharing_request_id'] = gem_sharing_request_id;

                //first we are "hiding" the existing copy, if it exists of cource.
                for (var k = 0; k < my_private_gems.length; k++) {
                    var obj_ID = my_private_gems[k]['gem_id'];
                    if (obj_ID == gem_id_from_element) {
                        my_private_gems.splice(k, 1); // removing 1 item from an array
                        break;
                    }
                }

                my_private_gems.push(private_elmnt);
                // }
            }

            // console.log( private_elmnt.body);
        }

        //  console.log(JSON.stringify(my_private_gems, null, 2));

        for (var i = 0; i < my_private_gems.length; i++) {
            delete my_private_gems[i]['user_id'];
        } //for security purposes


        var sorted_by_date_private_items = my_private_gems.sort(function(a, b) {
            var last_date_a = a['last_date'] ? a['last_date'] : 0;
            var last_date_b = b['last_date'] ? b['last_date'] : 0;
            //console.error("sorting date ",last_date_a,last_date_b, JSON.stringify(a, null, 2),JSON.stringify(b, null, 2));
            var results = last_date_b - last_date_a;
            return results;
        });
        
       
        return sorted_by_date_private_items;

    }
    else {
        //db error, do nothing
        return;

    }


}


async function getUserIdFromEmail({ email } = {}) {

    var params = {
        TableName: "users",
        IndexName: "email_index",
        KeyConditionExpression: "email_id = :email_id",
        ExpressionAttributeValues: { ":email_id": email }
    };

    try {
        const data = await docClient.query(params).promise();
        if (data.Items && data.Items.length == 1) {
            MegaLog({ data: data });
            var userInfo = data.Items[0];
            var userId = userInfo.user_id;
            if (typeof userId === "undefined") {
                MegaLogError({ error: data, comments: "No items found.USER IS NOT ACTIVATED", type: USER_FOR_SHARING_NOT_FOUND });
                return;
            }
            else {
                MegaLog({ data: "Found user_id:" + userId + " from email:" + email });
                // MegaLog({ data: data });
                return userId;
            }

        }
        else if (data.Items && data.Items.length > 1) {
            MegaLogError({ comments: "Too many items found.", type: USER_FOR_SHARING_NOT_FOUND });
            return;
        }
        else {
            MegaLogError({ error: data, comments: "No items found.", type: USER_FOR_SHARING_NOT_FOUND });
            return;
        }
    }
    catch (error) {
        MegaLogError({ params, error, comments: "!!", type: USER_FOR_SHARING_NOT_FOUND });
        return;
    }

    // return await getAllTags();

};

module.exports = function() {

    this.GetUserIdFromEmail = async({ email } = {}) => {
        return await getUserIdFromEmail({ email });
    };

    this.GetMyGems = async({ my_user_id } = {}) => {
        return await getMyGems({ my_user_id });
    };


    //  this.sum = function(a, b) { return a + b };
    this.createSharingRequest = async(user_email) => {

    };


    this.ConnectUser = async(my_email, my_sub_id, my_user_id) => {
        var alreadyActivated = await getUserPrefs(my_email, my_sub_id);
        //alreadyActivated = false; 
        if (alreadyActivated === true) {
            return "already activated";
        }
        else { // need activation
            const activatedUserPrefs = await ActivateUserPrefs(my_email, my_sub_id, my_user_id);
            if (activatedUserPrefs == true) {
                return "activated successfully";
            }
            else {
                return "there was a problem with activation";
            }
        }
    };
}
